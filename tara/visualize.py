"""
This package provides tools to visualize objects from `Trajectory` and `TrajectorySet` classes.
"""

from tara.trajectory import Trajectory
from tara.trajectory import TrajectorySet
import numpy as np
import matplotlib.pyplot as plt
from typing import List
from mpl_toolkits.mplot3d import Axes3D
from pyvtk import *

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=18)


def curv_tor_hist(sets : List[TrajectorySet], labels : List[str], Nbins : int = 1000, kappa_clip_min : float = 0, kappa_clip_max : float = 0.2, tau_clip_min : float = -5, tau_clip_max : float = 5, normed : bool = True, kappa_xlim : (float, float) = (0, 0.05), tau_xlim : (float, float) = (-0.3, 0.3), alpha : float = 0.5):
    """
    Plots a histogram over curvature (kappa) and torsion (tau) for a given set of `TrajectorySet`'s.

    Parameters
    ----------

    sets : List[TrajectorySet]
        Set of TrajectorySet's. Each of the sets will appear differently colored in the plot.
    labels : List[str]
        List of string with names (labels) of the TrajectorySet's in sets. This is used to name the plots in the graph.
    Nbins : int = 1000
        Number of bins für histogramm (default Nbins = 1000).
    kappa_clip_min : float = 0
        Curvature (kappa) can be clipped using numpy.clip() method. This value sets the lower limit (default = 0).
    kappa_clip_max : float = 0.2
        Curvature (kappa) can be clipped using numpy.clip() method. This value sets the upper limit (default = 0.2).
    tau_clip_min : float = -5
        Torsion (tau) can be clipped using numpy.clip() method. This value sets the lower limit (default = -5).
    tau_clip_max : float = 5
        Torsion (tau) can be clipped using numpy.clip() method. This value sets the upper limit (default = 5).
    normed : bool = True
        Norms the histogramm (default = True).
    kappa_xlim : (float, float) = (0, 0.04)
        Sets x limit for the curvature plot given as a tuple (default = (0, 0.02)).
    tau_xlim : (float, float) = (-0.3, 0.3)
        Sets x limit for the torsion plot given as a tuple (default = (-0,3, 0.3)).
    alpha : float = 0.5
        Sets alpha (transparency) for plots (default = 0.5).

    Returns
    =======
    """

    fig, axs = plt.subplots(1,2)

    for index, set in enumerate(sets):

        data = set.get_all

        axs[0].hist(np.clip(data[3], kappa_clip_min, kappa_clip_max), bins = Nbins, density=normed, label = labels[index], alpha=alpha)
        axs[1].hist(np.clip(data[4], tau_clip_min, tau_clip_max), bins = Nbins, density=normed, label = labels[index], alpha=alpha)

    axs[0].set_xlim(kappa_xlim[0], kappa_xlim[1])
    axs[0].legend()
    axs[1].set_xlim(tau_xlim[0], tau_xlim[1])

    axs[0].set_xlabel('$\\kappa$')
    axs[0].set_ylabel('$p(\\kappa)$')

    axs[1].set_xlabel('$\\tau$')
    axs[1].set_ylabel('$p(\\tau)$')

    fig.tight_layout()
    fig.show()
    plt.show()

def plot3d_trajectories(set : TrajectorySet):
    """
    Provides a overview over a set of trajectories by visualizing them as 3d plot.
    For many trajectories exporting them as vtk file is using Paraview for visualization.

    Parameters
    ----------
    set : TrajectorySet
        Set with data to visualize. Has to be a TrajectorySet object.
    N : int
        Number of trajectoies from the set, that should be displayed.
        Algorithm will select N evenly distributed trajectoies from set.

    Returns
    -------
    3d Plot of N trajectories from set.
    """

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    for i, traj in enumerate(set.trajectories):

        v = np.sqrt(traj.pos_dot_1[0]**2 + traj.pos_dot_1[1]**2 + traj.pos_dot_1[2]**2)
        x = traj.X
        y = traj.Y
        z = traj.Z

        ax.plot(x, y, z)

    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

def plot_trajectory(traj : Trajectory):

    traj.calc_torsion()

    fig, axs = plt.subplots(3,3)
    axs[0,0].plot(traj.time, traj.X)
    axs[0,0].set_ylabel(r'$x$')

    axs[0,1].plot(traj.time, traj.Y)
    axs[0,1].set_ylabel(r'$y$')

    axs[0,2].plot(traj.time, traj.Z)
    axs[0,2].set_ylabel('$z$')

    axs[1,0].plot(traj.time, traj.pos_dot_1[0])
    axs[1,0].set_ylabel(r'$\dot{x}$')

    axs[1,1].plot(traj.time, traj.pos_dot_1[1])
    axs[1,1].set_ylabel(r'$\dot{y}$')

    axs[1,2].plot(traj.time, traj.pos_dot_1[2])
    axs[1,2].set_ylabel(r'$\dot{z}$')

    axs[2,0].plot(traj.time, traj.pos_dot_2[0])
    axs[2,0].set_ylabel(r'$\ddot{x}$')

    axs[2,1].plot(traj.time, traj.pos_dot_2[1])
    axs[2,1].set_ylabel(r'$\ddot{y}$')

    axs[2,2].plot(traj.time, traj.pos_dot_2[2])
    axs[2,2].set_ylabel(r'$\ddot{z}$')

    axs[2,2].set_xlabel('Time $t$')
    plt.show()
    fig.show()

def export_vtk(set : TrajectorySet, fileName : str = 'vtk_set', coloring : str = 'velocity'):
    """
    Exports a given TrajectorySet as vtk file. This file allows proper visualization with Paraview.

    Parameters
    ----------
    set : TrajectorySet
        Set with data to visualize. Has to be a TrajectorySet object.
    fileName : str
        Name of the file you want to export.
    coloring : str
        Sets the scalar to color the trajectoies. Possible choices: 'velocity', 'acceleration', 'curvature', 'torsion'

    Returns
    -------
    vtk file.
    """

    lengths = []
    coloring_data = []
    points = []

    for tr in set.trajectories:
        pos = np.transpose(tr.positions).tolist()

        if coloring == 'acceleration':
            tr.calc_derivative(order = 2)
            coloring_data.extend(np.array(np.sqrt(tr.pos_dot_2[0] ** 2 + tr.pos_dot_2[1] ** 2 + tr.pos_dot_2[2] ** 2)))

        elif coloring == 'curvature':
            tr.calc_curvature()
            coloring_data.extend(tr.curvature.tolist())

        elif coloring == 'torsion':
            tr.calc_torsion()
            coloring_data.extend(tr.torsion.tolist())

        else :
            tr.calc_derivative(1)
            coloring_data.extend(np.array(np.sqrt(tr.pos_dot_1[0] ** 2 + tr.pos_dot_1[1] ** 2 + tr.pos_dot_1[2] ** 2)))

        points.extend(pos)
        lengths.append(len(tr.positions[0].tolist()))

    pointers =  [np.arange(0, lengths[0])]

    for l in lengths[1:-1]:
        pointers.append(np.arange(pointers[-1][-1] +1, pointers[-1][-1] +l +1))

    vtk = VtkData(\
            UnstructuredGrid(points, poly_line=pointers),
            PointData(Scalars(coloring_data, coloring)),
            'Unstructured Grid Example'
            )
    vtk.tofile(fileName,'binary')

def export_animated_vtk(set : TrajectorySet, fileName_prefix : str = 'vtk_set', coloring : str = 'velocity', tracer_length : int = 5):
    """
    Exports a given TrajectorySet as series of vtk files. This files allow proper visualization of flow with Paraview.

    Parameters
    ----------
    set : TrajectorySet
        Set with data to visualize. Has to be a TrajectorySet object.
    fileName : str
        Name of the file you want to export.
    coloring : str
        Sets the scalar to color the trajectoies. Possible choices: 'velocity', 'acceleration', 'curvature' or 'torsion'.

    Returns
    -------
    vtk file series.

    # TODO:     - Test for further bugs / stability issues (get rid of offset parameter!)
                - Select coloring does not work
                - Method needs to check if "coloring-parameter" is calculated in set.
    """
    offset = 4

    start_time = min([traj.time[0] for traj in set.trajectories])
    end_time = max([traj.time[-1] for traj in set.trajectories])

    for timestep in np.arange(start_time + tracer_length + offset, end_time +1):

        temp_trajectorySet = TrajectorySet()
        temp_trajectorySet.trajectories = []

        for traj in set.trajectories:

            # if coloring == 'acceleration':
            #     traj.calc_derivative(order = 2)
            # elif coloring == 'curvature':
            #     traj.calc_curvature()
            # elif coloring == 'torsion':
            #     traj.calc_torsion()
            # else:
            #     traj.calc_derivative(order = 1)

            if np.argwhere(traj.time == timestep) and np.argwhere(traj.time == timestep) >= offset:

                timestep_index = np.argwhere(traj.time == timestep).astype(int)[0,0]

                lower_timestep_index = max(0, timestep_index - tracer_length)

                temp_positions = traj.positions[:, lower_timestep_index : timestep_index]
                temp_times = traj.time[lower_timestep_index : timestep_index]

                if coloring == 'acceleration':
                    temp_trajectory = Trajectory(temp_positions,
                                                temp_times,
                                                pos_dot_2=traj.pos_dot_2[:, lower_timestep_index : timestep_index])
                elif coloring == 'curvature':
                    temp_trajectory = Trajectory(temp_positions,
                                                temp_times,
                                                pos_dot_2=traj.curvature[lower_timestep_index : timestep_index])
                elif coloring == 'torsion':
                    temp_trajectory = Trajectory(temp_positions,
                                                temp_times,
                                                pos_dot_2=traj.torsion[lower_timestep_index : timestep_index])
                else:
                    temp_trajectory = Trajectory(temp_positions,
                                                temp_times,
                                                pos_dot_1=traj.pos_dot_1[:, lower_timestep_index : timestep_index])

                temp_trajectorySet.trajectories.append(temp_trajectory)

        if len(temp_trajectorySet.trajectories) > 0:
            export_vtk(temp_trajectorySet, f'{fileName_prefix}_{timestep}', coloring = coloring)
