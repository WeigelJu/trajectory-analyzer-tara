from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.visualize import *
import numpy as np
import matplotlib.pyplot as plt
import scipy as sci
import scipy.signal as sig
import time
import os

# set = TrS()
# set.import_goettingen_file('tara/temp_files/tracks_000001.npy')
#
#
# set.trajectories = [traj for traj in set.trajectories if len(traj.time) >= 273]
#
# set.calc_derivative(order=2)
# set.calc_curvature()
# set.calculate_curvature_torsion()
# set.calc_cgl_acceleration(sigma=40)


rootdir = 'E:/0.5Hz_long_series'

filelist = list()
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        filelist.append((file, os.path.join(subdir, file)))

# Remove those files from list that do not follow naming convention for raw data.
filelist = [file for file in filelist if file[0].endswith('.npy') and file[0].startswith('tracks_')]

filelist[0][1].replace('\\', '/')
