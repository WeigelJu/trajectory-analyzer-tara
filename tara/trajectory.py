"""
This module provides tools to read and analyze data generated in "TBL Experiment" accomplished by DLR in Goettingen.

"""

import numpy as np
import scipy.interpolate as interpolate
import matplotlib.pyplot as plt
import scipy.signal as sig
import math
from typing import List
import h5py
import warnings
import time
import pkg_resources
import os

from tara.progressbar import printProgressBar

class Trajectory:
    """
    Trajectory is a data type made to handle experimental data and fit them with splines using ´scipy´ package.

    See Also
    --------

    TrajectorySet : Returns a set of `Trajectory` items.

    Examples
    --------
    >>> from tara.trajectory import Trajectory
    >>> import numpy as np

    >>> X = np.array([1,2,3,4])
    >>> Y = np.array([6,5,4,2])
    >>> Z = np.array([1,3,5,7])
    >>> time = np.array([0,1,2,3])
    >>> pos = np.array([X, Y, Z])

    Creating a `Trajectory` object with `pos` and `time`:

    >>> traj = Trajectory(pos, time)
    >>> traj.positions
    array([[1, 2, 3, 4],
           [6, 5, 4, 2],
           [1, 3, 5, 7]])
    >>> traj.calc_torsion()
    array([-2.07241631e-16, -4.38382077e+14, -2.07241631e-16, -6.66133815e-17])

    """

    def __init__(self, positions : np.ndarray, time : np.ndarray, pos_dot_1 : np.ndarray = None, pos_dot_2 : np.ndarray = None, pos_dot_3 : np.ndarray = None,
                 cgl_acceleration : np.ndarray = None, curvature : np.ndarray = None, torsion : np.ndarray = None, smoothness : float = 0, clip : int = 0):
        """
        Trajectory of one single particle.

        Parameters
        ----------
        positions : numpy.ndarray
            Numpy array has to have shape np.array([[X], [Y], [Z]]) with [X], [Y] and [Z] the time series of positions.
        time      : numpy.ndarray
            Numpy array with time information. This is a frame counter, so conversion with the framerate of the camera is needed to calculate a real time information.
        pos_dot_1 : numpy.ndarray
            With shape like 'positions'. Contains time series of first derivative (default = None).
        pos_dot_2 : numpy.ndarray
            With shape like 'positions'. Contains time series of second derivative (default = None).
        pos_dot_3 : numpy.ndarray
            With shape like 'positions'. Contains time series of third derivative (default = None).
        curvature : numpy.ndarray
            With shape like 'time'. Contains time series of curvature (default = None).
        torsion   : numpy.ndarray
            With shape like 'time'. Contains time series of torsion (default = None).
        smoothness : float
            Parameter of smoothness (default = 0) for `scipy.interpolate` spline fits used in this package.
        clip : int
            Number of elements that are "skipped" at both ends of all positions, derivatives, curvature etc. This is due to inaccuracies
            at the beginning of the data set (default = 5).

        Returns
        -------
        out : object of class Trajectory.

        """
        assert positions.shape[0] == 3
        assert positions.shape[1] == time.shape[0]
        assert positions.shape[1] >= 2 * clip

        self._positions  :   np.float32     =   positions
        self._time       :   np.float32     =   time
        self._pos_dot_1  :   np.float32     =   pos_dot_1
        self._pos_dot_2  :   np.float32     =   pos_dot_2
        self._pos_dot_3  :   np.float32     =   pos_dot_3
        self._cgl_acceleration : np.float32 =   cgl_acceleration
        self._curvature  :   np.float32     =   curvature
        self._torsion    :   np.float32     =   torsion
        self._smoothness :   float          =   smoothness
        self._clip       :   int            =   clip


    @property
    def clip(self):
        """
        Returns clip value of the Trajectory object.
        """
        return self._clip


    @clip.setter
    def clip(self, value : int):
        """
        Sets new clip value to Trajectory object.
        """
        self._clip = value


    @property
    def positions(self):
        """
        Returns all positions of this trajectory.

        Returns
        -------
        out : Numpy array np.array([[X], [Y], [Z]]) with time series.

        """
        if self.clip == 0:
            return self._positions
        else:
            return self._positions[:,self.clip:-self.clip]


    @property
    def time(self):
        """
        Returns array with time informations.

        Returns
        -------
        out : Numpy array with time series.
        """
        if self.clip == 0:
            return self._time

        return self._time[self.clip:-self.clip]


    @property
    def pos_dot_1(self):
        """
        Returns 1. derivatives
        """
        if self.clip == 0:
            return self._pos_dot_1

        return self._pos_dot_1[:,self.clip:-self.clip]


    @pos_dot_1.setter
    def pos_dot_1(self, vals : np.float32):
        """
        Writes 1. derivatives
        """
        assert vals.shape == self._positions.shape
        self._pos_dot_1 = vals


    @property
    def pos_dot_2(self):
        """
        Returns 2. derivatives
        """
        if self.clip == 0:
            return self._pos_dot_2

        return self._pos_dot_2[:,self.clip:-self.clip]


    @pos_dot_2.setter
    def pos_dot_2(self, vals : np.float32):
        """
        Writes 2. derivatives
        """
        assert vals.shape == self._positions.shape
        self._pos_dot_2 = vals


    @property
    def pos_dot_3(self):
        """
        Returns 3. derivatives
        """
        if self.clip == 0:
            return self._pos_dot_3

        return self._pos_dot_3[:,self.clip:-self.clip]


    @pos_dot_3.setter
    def pos_dot_3(self, vals : np.float32):
        """
        Writes 3. derivatives
        """
        assert vals.shape == self._positions.shape
        self._pos_dot_3 = vals


    @property
    def cgl_acceleration(self):
        """
        Returns Coarse-grained Lagrangian acceleration.
        """
        if self.clip == 0:
            return self._cgl_acceleration

        return self._cgl_acceleration[self.clip:-self.clip]


    @cgl_acceleration.setter
    def cgl_acceleration(self, vals : np.float32):
        """
        Writes Coarse-grained Lagrangian acceleration.
        """
        assert vals.shape == self._time.shape
        self._cgl_acceleration = vals


    @property
    def curvature(self):
        """
        Returns curvature

        Returns
        -------
        out : Numpy array with time series of curvature.
        """
        if self.clip == 0:
            return self._curvature

        return self._curvature[self.clip:-self.clip]


    @curvature.setter
    def curvature(self, curv : np.float32):
        """
        Writes curvature
        """
        assert curv.shape == self.time.shape
        self._curvature = curv


    @property
    def torsion(self):
        """
        Returns torsion
        """
        if self.clip == 0:
            return self._torsion

        return self._torsion[self.clip:-self.clip]


    @torsion.setter
    def torsion(self, tor : np.float32):
        """
        Writes curvature
        """
        assert tor.shape == self.time.shape
        self._torsion = tor


    @property
    def X(self):
        """
        Returns time series of X-Koordinates
        """
        return self.positions[0]


    @property
    def Y(self):
        """
        Returns time series of Y-Koordinates
        """
        return self.positions[1]


    @property
    def Z(self):
        """
        Returns time series of Z-Koordinates
        """
        return self.positions[2]


    @property
    def len(self):
        """
        Returns time series of Z-Koordinates
        """
        return len(self.time)


    def calc_derivative(self, order : int, force_calculation : bool = False):
        """
        Calculates n'th (max 3) oder of derivative and writes it into ´pos_dot_x´ field in the instance of the Trajectory class.

        # TODO: Explain how smoothness is used in BSpline fit and its length dependency.

        Parameter
        ---------

        order : int
            Set `order = 1` for first derivative, `order = 2` for second derivative and so on.
        force_calculation : bool
            If this is 'True' all the derivative will we calculated whether or not the derivative allready exists.
            Use this to recalculate derivatives after changes in parameters like 'smoothness'.

        """
        smooth = lambda s: self.len*s**2 if s >= 0 else -1*s

        t_x, c_x, k_x = interpolate.splrep(self._time, self._positions[0], k=3, s=smooth(self.smoothness))
        t_y, c_y, k_y = interpolate.splrep(self._time, self._positions[1], k=3, s=smooth(self.smoothness))
        t_z, c_z, k_z = interpolate.splrep(self._time, self._positions[2], k=3, s=smooth(self.smoothness))

        spline_x = interpolate.BSpline(t_x, c_x, k_x, extrapolate=False)
        spline_y = interpolate.BSpline(t_y, c_y, k_y, extrapolate=False)
        spline_z = interpolate.BSpline(t_z, c_z, k_z, extrapolate=False)

        if order >= 1 and (self._pos_dot_1 is None or force_calculation is True):
            self._pos_dot_1 = np.array([spline_x.derivative(1)(self._time), spline_y.derivative(1)(self._time), spline_z.derivative(1)(self._time)], dtype=np.float32)
        if order >= 2 and (self._pos_dot_2 is None or force_calculation is True):
            self._pos_dot_2 = np.array([spline_x.derivative(2)(self._time), spline_y.derivative(2)(self._time), spline_z.derivative(2)(self._time)], dtype=np.float32)
        if order >= 3 and (self._pos_dot_3 is None or force_calculation is True):
            self._pos_dot_3 = np.array([spline_x.derivative(3)(self._time), spline_y.derivative(3)(self._time), spline_z.derivative(3)(self._time)], dtype=np.float32)


    def calc_cgl_acceleration(self, sigma : float):
        """
        Calculates the 'Coarse-grained Lagrangian acceleration' of a trajectory.
        """
        self.calc_derivative(order = 2)
        a_square = self._pos_dot_2[0]**2 + self._pos_dot_2[1]**2 + self._pos_dot_2[2]**2
        alpha = sig.convolve(a_square, sig.gaussian( len(self._time), sigma), 'same')
        self._cgl_acceleration = np.array(alpha, dtype=np.float32)


    def mean_kinetic_energy(self, t0, tau):
        """
        Calculates the mean kinetic energy for a particle in a given time (frame) interval t0.

        Parameters
        ----------

        t0 : int
            Starting time of the time interval

        tau : int
            Number of frames the kinetic energy is averaged over.
        """
        if self.time[0] <= t0 and self.time[-1] >= t0:

            t0_index = np.argwhere(self.time == t0)

            if tau + t0_index <= len(self.time):
                return 1/2 * np.mean(self.pos_dot_1[0]**2 + self.pos_dot_1[1]**2 + self.pos_dot_1[2]**2)


        return -1


    def velocity_increment(self, tau : int):
        """
        Calculates the velocity increment del_u = u(t) - u(t+tau)
        """
        if tau < self.len and tau > 0:
            del_ux = self.pos_dot_1[0][:-tau] - self.pos_dot_1[0][tau:]
            del_uy = self.pos_dot_1[1][:-tau] - self.pos_dot_1[1][tau:]
            del_uz = self.pos_dot_1[2][:-tau] - self.pos_dot_1[2][tau:]

            return del_ux, del_uy, del_uz


    def _acceleration_acf_coefficient(self, tau : int):
        """
        Calculates the one produkt c_(tau) = a(t_mid - tau/2) * a(t_mid + tau/2) of correlation funktion.

        # TODO: Add different "select modes".
        """
        if tau < self.len and tau >= 0:

            if self.pos_dot_2 is None:
                self.calc_derivative(order=2)

            center = int(self.len/2)

            # Offset index needs to be rouded up!
            offset = int(math.ceil(tau/2))

            c_x = self.pos_dot_2[0][center - offset] * self.pos_dot_2[0][center - offset + tau]
            c_y = self.pos_dot_2[1][center - offset] * self.pos_dot_2[1][center - offset + tau]
            c_z = self.pos_dot_2[2][center - offset] * self.pos_dot_2[2][center - offset + tau]

            return c_x, c_y, c_z


    def acceleration_acf_centered(self, tau_max : int):
        """
        Calculates all possible produkts for 1 < tau < min( tau_max, traj.len).

        Parameters
        ----------
        tau_max : int
            Maximum tau for witch produkts c_(tau) = a(t_mid - tau/2) * a(t_mid + tau/2) with 1 <= tau <= tau_minmax will be calculated.

        Returns
        -------
        out : tuple of numpy.ndarrays.
            out[0] is x-component, out[1] is y component and out[2] is z-component.
            out[0][0] contains value of c_(tau=1) = a_x(t_mid - 1) * a_x(t_mid),
            out[0][1] contains value of c_(tau=2) = a_x(t_mid - 1) * a_x(t_mid + 1),
            and so on.

        Examples
        --------

        >>> import numpy as np
        >>> from tara.trajectory import Trajectory as Tr
        >>> from tara.trajectory import TrajectorySet as TrS

        Creating simple test data.
                                                   mid
                                                    v
        >>> example_time = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        >>> example_positions = np.array([example_time, example_time, example_time])

        Create a Trajectory object with with test data.

        >>> tr = Tr(example_positions, example_time)

        Writing test data to first and second derivative

        >>> tr.pos_dot_1 = example_positions
        >>> tr.pos_dot_2 = example_positions

        Calculating all 27 possible auto correlation functions produkts for this trajectory saved in 3 arrays (one for each component).

        >>> tr.acceleration_acf(9)
        (array([20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]), array([20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]), array([20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
        """

        tau_minmax = min(tau_max, self.len -1)

        if tau_minmax >= 0:

            result_x = np.zeros(tau_minmax +1)
            result_y = np.zeros(tau_minmax +1)
            result_z = np.zeros(tau_minmax +1)

            for tau in range(0, tau_minmax +1):
                c_x, c_y, c_z = self._acceleration_acf_coefficient(tau)
                result_x[tau] = c_x
                result_y[tau] = c_y
                result_z[tau] = c_z

            return result_x, result_y, result_z


    def acceleration_acf_mean(self, tau_max : int):

        tau_minmax = min(tau_max, self.len -1)

        if tau_minmax > 0:

            result_x = np.zeros(tau_minmax +1)
            result_y = np.zeros(tau_minmax +1)
            result_z = np.zeros(tau_minmax +1)

            u_x, u_y, u_z = self.pos_dot_2

            for tau in range(0, tau_minmax +1):

                if tau is 0:
                    c_x = np.mean(u_x * u_x)
                    c_y = np.mean(u_y * u_y)
                    c_z = np.mean(u_z * u_z)
                else:
                    c_x = np.mean(u_x[tau:] * u_x[:-tau])
                    c_y = np.mean(u_y[tau:] * u_y[:-tau])
                    c_z = np.mean(u_z[tau:] * u_z[:-tau])

                result_x[tau] = c_x
                result_y[tau] = c_y
                result_z[tau] = c_z

            return result_x, result_y, result_z


    def calc_curvature(self, force_calculation : bool = False):
        """
        Calculates the time series of curvature for this trajectory using spline fits.
        """
        self.calc_derivative(order = 2, force_calculation = force_calculation)

        if self._curvature is None or force_calculation:

            u_in_u = self._pos_dot_1[0]**2 + self._pos_dot_1[1]**2 + self._pos_dot_1[2]**2

            udot_in_udot = self._pos_dot_2[0]**2 + self._pos_dot_2[1]**2 + self._pos_dot_2[2]**2

            t_in_udot_squared = (1/np.sqrt(u_in_u) * (self._pos_dot_1[0]*self._pos_dot_2[0] + self._pos_dot_1[1] * self._pos_dot_2[1] + self._pos_dot_1[2] * self._pos_dot_2[2]))**2

            curvature = np.sqrt(udot_in_udot - t_in_udot_squared)/u_in_u

            self._curvature = curvature


    def calc_torsion(self, force_calculation : bool = False):
        """
        Calculates the time series of torsion for this trajectory using spline fits.
        """
        if self._curvature is None:
            self.calc_curvature(force_calculation = force_calculation)

        self.calc_derivative(order = 3, force_calculation = force_calculation)

        if self._torsion is None or force_calculation:

            u_in_u = self._pos_dot_1[0]**2 + self._pos_dot_1[1]**2 + self._pos_dot_1[2]**2

            numerator = self._pos_dot_1[0] * self._pos_dot_2[1] * self._pos_dot_3[2] - self._pos_dot_1[0] * self._pos_dot_2[2] * self._pos_dot_3[1] + \
                        self._pos_dot_1[1] * self._pos_dot_2[2] * self._pos_dot_3[0] - self._pos_dot_1[1] * self._pos_dot_2[0] * self._pos_dot_3[2] + \
                        self._pos_dot_1[2] * self._pos_dot_2[0] * self._pos_dot_3[1] - self._pos_dot_1[2] * self._pos_dot_2[1] * self._pos_dot_3[0]

            torsion = numerator/(u_in_u**3 * self._curvature**2)

            self._torsion = torsion


    @property
    def smoothness(self):
        """
        Returns smoothness factor. This factor can only been changed using the change_smoothness() method.
        """
        return self._smoothness


    @smoothness.setter
    def smoothness(self, val : int):
        """
        Changes the smoothness factor for spline-fits and recalculates curvature and torsion, if they have already been calculated.
        """
        self._smoothness = val
        self.recalculate()


    def recalculate(self):
        """
        Recalculates all derivatives, curvature, torsion...
        """
        if self._pos_dot_1 is not None:
            self.calc_derivative(order=1, force_calculation=True)
        if self._pos_dot_2 is not None:
            self.calc_derivative(order=2, force_calculation=True)
        if self._pos_dot_3 is not None:
            self.calc_derivative(order=3, force_calculation=True)

        if self._curvature is not None:
            self.calc_curvature(force_calculation=True)
        if self._torsion is not None:
            self.calc_torsion(force_calculation=True)


    def add_noise(self):
        """
        Adds a (random) exponentially distributed noise to ALL position data and recalculates curvature and torsion, if they have already been calculated.
        """
        raise NotImplementedError


class TrajectorySet:
    """
    TrajectorySet is class to handle a set of trajectory

    Parameters
    ----------

    trajectories: List[Trajectory]
        List of trajectories from class 'Trajectory' (default = [] empty List).

    Returns
    -------

    out : object of class TrajectorySet.

    See Also
    --------
    Trajectory : Returns a 'Trajectory' item.

    Examples
    --------
    >>> from tara.trajectory import Trajectory, TrajectorySet
    >>> import numpy as np
    >>> set = TrajectorySet()
    >>> set.import_goettingen_file('tara/temp_files/test_tracks.npy', smooth=10)
    >>> set.calculate_curvature_torsion()
    >>> set.trajectories[0]
    <tara.trajectory.Trajectory object at 0x00000176DAC6A438>
    >>> np.mean(set.trajectories[0].curvature)
    0.001448944553858971

    This example reads data from 'tara/temp_files/test_tracks.npy' file into a `TrajectorySet` object and calculates curvature and torsion for all `Trajectory` objects in `TrajectorySet`.
    """

    def __init__(self, trajectories : List[Trajectory] = [], verbose : bool = True):
        self._trajectories = trajectories
        self._verbose = verbose


    @property
    def trajectories(self):
        """
        Returns the set of trajectories
        """
        return self._trajectories


    @trajectories.setter
    def trajectories(self, trajs : List[Trajectory]):
        """
        Sets a set of trajectories
        """
        self._trajectories = trajs


    @property
    def verbose(self):
        """
        Returns verbose : bool parameter of the TrajectorySet.
        """
        return self._verbose


    @verbose.setter
    def verbose(self, value : bool):
        """
        Sets verbose : bool parameter of the TrajectorySet to given value :bool.
        """
        self._verbose = value


    @property
    def smoothness(self):
        """
        Returns smoothness factor of trajectories. If they are inconsistent, a waring is printed.
        """
        consistent = True
        reference = self.trajectories[0].smoothness
        for trajectory in self.trajectories:
            if trajectory.smoothness != reference:
                consistent = False

        if not consistent:
            print(f'Warning: smoothness is not consistent in this set! Value of the first trajectory in set has been returned!')
        return reference


    @smoothness.setter
    def smoothness(self, value : int):
        """
        Sets new smoothness for all trajectories in the set. All derivatives, the curvature and the torsion will be recalculated to apply the chances.
        """
        # print('Updating smoothness for TrajectorySet!')
        for index, trajectory in enumerate(self.trajectories):
            trajectory.smoothness = value

            if self.verbose and self.trajectories:
                if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                    printProgressBar(index+1, len(self.trajectories), prefix=f'Updating smoothness of TrajectorySet:', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)


    @property
    def len(self):
        """
        Returns length of trajectories list (number of trajectories in set).
        """
        return len(self.trajectories)


    def load_raw(self, file : str, smooth : float = 0, append : bool = False):
        """
        Reads a numpy file with trajectories from Goettingen and returns a List of Trajectory elements.

        Parameters
        ----------

        file : str
            Path and file name.
        smooth : int
            smoothness parameter applied to all Trajectory (see Trajectory.smoothness).

        # TODO: Documentation of append!

        Examples
        --------

        >>> from tara.trajectory import Trajectory, TrajectorySet
        >>> set = TrajectorySet()
        >>> set.import_goettingen_file('test_tracks.npy', smooth=10)
        >>> set.calculate_curvature_torsion()
        Calculating curvature and torsion: 0 of 469 => 0% done!
        Calculating curvature and torsion: 47 of 469 => 10% done!
        ...
        Calculating curvature and torsion: 423 of 469 => 90% done!
        >>> set.trajectories[0].curvature
        array([3.39129015e-03, 2.85590300e-03, 2.33516888e-03, 1.83664177e-03,
                ...
               6.73113912e-03])

        """

        data = np.load(file, encoding='bytes', allow_pickle=True)

        trajectories : List[Trajectory] = []

        for particle in data:
            trajectories.append(Trajectory( np.array([particle[0], particle[1], particle[2]]), particle[-1], smoothness = smooth))

        if append and trajectories:
            self.trajectories.extend(trajectories)
        else:
            self.trajectories = trajectories


    def import_goettingen_file(self, file : str, smooth : int = 0):
        self.load_raw(file, str, smooth)


    def calc_derivative(self, order : int, force_calculation : bool = False):
        """
        Calculates order'th derivative for ALL trajectories in the set.

        Parameters
        ==========

        force_calculation : bool
            If this is set calculation is forced regardless of existing derivatives values.
        """

        for index, traj in enumerate(self.trajectories):
            traj.calc_derivative(order, force_calculation)

            if self.verbose and self.trajectories:
                if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                    printProgressBar(index+1, len(self.trajectories), prefix=f'Calculating derivative (oder {order}):', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)


    def calc_cgl_acceleration(self, sigma : float):
        """
        Calculates the 'Coarse-grained Lagrangian acceleration' for all trajectory.

        Parameters
        ==========

        sigma : float
            Standard deviation of the gaussian cernel. Important: This is the number of frames instead of real time.
        """

        for index, traj in enumerate(self.trajectories):
            traj.calc_cgl_acceleration(sigma)

            if self.verbose and self.trajectories:
                if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                    printProgressBar(index+1, len(self.trajectories), prefix=f'Calculating cgl_acceleration with sigma={sigma}:', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)


    def mean_kinetic_energy(self, t0, tau):
        """
        Calculates the mean kinetic energy of a particle in a given time (frame) interval t0.

        Parameters
        ==========

        t0 : int
            Starting time of the time interval

        tau : int
            Number of frames the kinetic energy is averaged over.
        """

        energies = [traj.mean_kinetic_energy(t0, tau) for traj in self.trajectories if traj.mean_kinetic_energy(t0, tau) is not -1]
        return energies


    def acceleration_acf_centered(self, tau_max : int):
        """
        Calculates the acceleration correlation funktion C_(tau) = a(t) * a(t+tau)

        # TODO: All documentation!
        """
        acf_container_x = [list() for _ in range(tau_max +1)]
        acf_container_y = [list() for _ in range(tau_max +1)]
        acf_container_z = [list() for _ in range(tau_max +1)]

        for traj in self.trajectories:

            cx, cy, cz = traj.acceleration_acf_centered(tau_max)

            for i, val in enumerate(cx):
                acf_container_x[i].append(val)

            for i, val in enumerate(cy):
                acf_container_y[i].append(val)

            for i, val in enumerate(cz):
                acf_container_z[i].append(val)

        return acf_container_x, acf_container_y, acf_container_z


    def acceleration_acf_mean(self, tau_max : int):
        """
        Calculates the acceleration correlation funktion C_(tau) = a(t) * a(t+tau)

        # TODO: All documentation!
        """
        acf_container_x = [list() for _ in range(tau_max +1)]
        acf_container_y = [list() for _ in range(tau_max +1)]
        acf_container_z = [list() for _ in range(tau_max +1)]

        for traj in self.trajectories:

            cx, cy, cz = traj.acceleration_acf_mean(tau_max)

            for i, val in enumerate(cx):
                acf_container_x[i].append(val)

            for i, val in enumerate(cy):
                acf_container_y[i].append(val)

            for i, val in enumerate(cz):
                acf_container_z[i].append(val)

        return acf_container_x, acf_container_y, acf_container_z


    def calc_curvature(self, force_calculation : bool = False):
        """
        Calculates curvature for ALL trajectories in the set.

        Parameters
        ==========

        force_calculation : bool
            If this is set calculation is forced regardless of existing derivatives or curvature values.
        """

        for index, traj in enumerate(self.trajectories):
            traj.calc_curvature(force_calculation)

            if self.verbose and self.trajectories:
                if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                    printProgressBar(index+1, len(self.trajectories), prefix=f'Calculating curvature:', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)


    def velocity_increment(self, tau : int, bins : np.ndarray = None):
        """
        Calculats velocity increments delta_u (t, tau) = u(t) - u(t + tau) for a given tau.

        # TODO: Explain bins mechanic. Due to massive memory usage it is smart do calculate the histogramm with given bins right in the method.

        Parameters
        ==========
        tau : int
            Number of time (frame) steps.

        Returns
        =======
        out : list()
            List with velocity increments for all t with length of trajectory minus tau. out[0] for x-, out[1] for y- and out[2] for z components.
        """
        del_ux = list()
        del_uy = list()
        del_uz = list()


        for index, traj in enumerate(self.trajectories):
            tr_dels = traj.velocity_increment(tau)

            if tr_dels:
                del_ux.extend(tr_dels[0])
                del_uy.extend(tr_dels[1])
                del_uz.extend(tr_dels[2])

        if bins is not None:

            bins_x = bins * np.sqrt(np.mean(np.asarray(del_ux)**2))
            bins_y = bins * np.sqrt(np.mean(np.asarray(del_uy)**2))
            bins_z = bins * np.sqrt(np.mean(np.asarray(del_uz)**2))

            h_del_ux = np.histogram(del_ux, bins=bins_x)[0]
            h_del_uy = np.histogram(del_uy, bins=bins_y)[0]
            h_del_uz = np.histogram(del_uz, bins=bins_z)[0]

            return h_del_ux, h_del_uy, h_del_uz
        else:
            return del_ux, del_uy, del_uz



    def calculate_curvature_torsion(self, force_calculation : bool = False):
        """
        Calculates curvature and torsion for ALL trajectories in the set.

        Parameters
        ==========

        force_calculation : bool
            If this is set calculation is forced regardless of existing derivatives or torion values.
        """

        for index, traj in enumerate(np.asarray(self.trajectories)):
            traj.calc_torsion(force_calculation)

            if self.verbose and self.trajectories:
                if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                    printProgressBar(index+1, len(self.trajectories), prefix=f'Calculating curvature and torsion:', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)


    def save_hdf5(self, file : str, append = False):
        """
        Saves all trajectories in a hdf5 file.

        Parameters
        ==========
        file : str
            File path + filename as string.
        append : bool
            If append is set True, the trajectories in will be added to an existing file. Append =  False will overwrite an existing file. (default = False)
        """

        index_offset = 0

        if append and os.path.isfile(f'{file}'):
            output_file = h5py.File(f'{file}', 'r+', libver='latest')
            key_indices = [int(key) for key in output_file.keys()]

            if key_indices:
                index_offset = max(key_indices) + 1

        else:
            output_file = h5py.File(f'{file}', 'w', libver='latest')

        try:
            output_file.attrs['version'] = pkg_resources.get_distribution("trajectory-analyzer-TARA").version

            for index, trajectory in enumerate(np.asarray(self.trajectories)):

                if self.verbose and self.trajectories:
                    if (index+1)% round(len(self.trajectories)/50) == 0 or (index+1) == len(self.trajectories):
                        printProgressBar(index+1, len(self.trajectories), prefix=f'Writing hdf5 file:', suffix=f'{index+1} of {len(self.trajectories)} done.', length=50)

                current_particle = output_file.create_group(f'{index + index_offset}')

                current_particle.attrs['smoothness'] = trajectory._smoothness
                current_particle.create_dataset(f'positions', data = trajectory._positions)
                current_particle.create_dataset(f'time', data = trajectory._time)

                if trajectory._pos_dot_1 is None:
                    current_particle.attrs['pos_dot_1'] = False
                else:
                    current_particle.attrs['pos_dot_1'] = True
                    current_particle.create_dataset(f'pos_dot_1', data = trajectory._pos_dot_1)

                if trajectory._pos_dot_2 is None:
                    current_particle.attrs['pos_dot_2'] = False
                else:
                    current_particle.attrs['pos_dot_2'] = True
                    current_particle.create_dataset(f'pos_dot_2', data = trajectory._pos_dot_2)

                if trajectory._pos_dot_3 is None:
                    current_particle.attrs['pos_dot_3'] = False
                else:
                    current_particle.attrs['pos_dot_3'] = True
                    current_particle.create_dataset(f'pos_dot_3', data = trajectory._pos_dot_3)

                if trajectory._curvature is None:
                    current_particle.attrs['curvature'] = False
                else:
                    current_particle.attrs['curvature'] = True
                    current_particle.create_dataset(f'curvature', data = trajectory._curvature)

                if trajectory._torsion is None:
                    current_particle.attrs['torsion'] = False
                else:
                    current_particle.attrs['torsion'] = True
                    current_particle.create_dataset(f'torsion', data = trajectory._torsion)

                if trajectory._cgl_acceleration is None:
                    current_particle.attrs['cgl_acceleration'] = False
                else:
                    current_particle.attrs['cgl_acceleration'] = True
                    current_particle.create_dataset(f'cgl_acceleration', data = trajectory._cgl_acceleration)
        except:
            print(f'Something went wrong while writing hdf5 file: {file}. Output file closed!')

        output_file.attrs['length'] = index + index_offset
        output_file.close()


    def load_hdf5(self, file : str, overwrite : bool = False):
        """
        # TODO: Documentation
        """

        input_file = h5py.File(f'{file}', 'r')
        print(input_file.attrs['version'])

        if not input_file.attrs['version'] == pkg_resources.get_distribution("trajectory-analyzer-TARA").version:
            file_version = input_file.attrs['version']
            warnings.warn(f'Version missmatch found! This file has been created with Tara {file_version}. You are currently using version {pkg_resources.get_distribution("trajectory-analyzer-TARA").version}')

        if overwrite:
            self.trajectories = []

        for index, trajectory in enumerate(input_file.values()):

            if index % round(len(input_file.keys())/50) == 0 and self.verbose:
                printProgressBar(index+1, len(input_file.keys()), prefix=f'Importing hdf5 file:', suffix=f'{index+1} of {len(input_file.keys())} done.', length=50)

            t = trajectory['time']
            smooth = trajectory.attrs['smoothness']
            pos = np.asarray(trajectory['positions'])


            if trajectory.attrs['pos_dot_1']:
                pos_dot_1 = np.asarray(trajectory['pos_dot_1'])
            else:
                pos_dot_1 = None

            if  trajectory.attrs['pos_dot_2']:
                pos_dot_2 = np.asarray(trajectory['pos_dot_2'])
            else:
                pos_dot_2 = None

            if  trajectory.attrs['pos_dot_3']:
                pos_dot_3 = np.asarray(trajectory['pos_dot_3'])
            else:
                pos_dot_3 = None

            if trajectory.attrs['curvature']:
                curv = np.asarray(trajectory['curvature'])
            else:
                curv = None

            if trajectory.attrs['torsion']:
                tor = np.asarray(trajectory['torsion'])
            else:
                tor = None

            if trajectory.attrs['cgl_acceleration']:
                cgl_acceleration = np.asarray(trajectory['cgl_acceleration'])
            else:
                cgl_acceleration = None

            self.trajectories.append(Trajectory(positions = pos,
                                                time = t,
                                                pos_dot_1 = pos_dot_1,
                                                pos_dot_2 = pos_dot_2,
                                                pos_dot_3 = pos_dot_3,
                                                curvature = curv,
                                                torsion = tor,
                                                cgl_acceleration = cgl_acceleration,
                                                smoothness = smooth))

        input_file.close()


    def save_npz(self, file : str, slim : bool = False):
        """
        Writes all trajectories with all contained information into a compressed numpy file.

        Parameter
        ---------
        file : str
            File name with path.
        slim : bool
            if slim is set True (default), trajectories from TrajectorySet will be saved without derivatives (`pos_dot_1`, `pos_dot_2`, `pos_dot_2` = None). This decreases file size by up to 70%.

        Examples
        --------
        >>> from tara.trajectory import Trajectory, TrajectorySet
        >>> set = TrajectorySet()
        >>> set.import_goettingen_file('test_tracks.npy', smooth=10)
        >>> set.calculate_curvature_torsion()
        Calculating curvature and torsion: 0 of 469 => 0% done!
        Calculating curvature and torsion: 47 of 469 => 10% done!
        ...
        Calculating curvature and torsion: 423 of 469 => 90% done!
        >>> set.save_npz('test_tracks_processed', slim=True)
        """

        if slim:
            copy = self.trajectories.copy()

            for traj in copy:
                traj._pos_dot_1 = None
                traj._pos_dot_2 = None
                traj._pos_dot_3 = None

            np.savez_compressed(f'{file}', np.asarray(copy))

        else:
            np.savez_compressed(f'{file}', np.asarray(self.trajectories))


    def load_npz(self, file):
        """
        Reads all trajectories from file to this TrajectorySet.

        Parameter
        ---------
        file : str
            File name with path.

        Examples
        --------
        >>> from tara.trajectory import Trajectory, TrajectorySet
        >>> set = TrajectorySet()
        >>> set.load('trajectory_file.npz')
        >>> set.trajectories[0].curvature
        array([0.19987101, 0.14927645, 0.1832035 , 0.29759379])

        """
        self.trajectories = np.load(f'{file}', allow_pickle=True)['arr_0']
