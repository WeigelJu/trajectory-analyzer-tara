from setuptools import setup

setup(name="Trajectory Analyzer TARA",
      version="1.0.0",
      description="Tool to read and analyze trajectory data from experiments in Goettingen",
      url="https://bitbucket.org/WeigelJu/trajectory-analyzer-tara/",
      author="Julian Weigel",
      author_email="julian.weigel@physik.uni-marburg.de",
      license="MIT",
      packages=["tara"],
      install_requires=[
          "numpy",
          "scipy",
          "matplotlib",
          "h5py",
          "pyvtk",
          "typing",
      ],
      entry_points = {
        "console_scripts": ['tara=tara.command_line:main'],
      },
      zip_safe=False)
