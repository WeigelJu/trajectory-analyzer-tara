# Trajectory Analyzer (TARA)

Tool to read and analyze data generated in "TBL Experiment" by DLR in Goettingen. This tool provides the classed **Trajectory** and **TrajectorySet** allowing simple handling of the data.

### TOTO: Tests of relevant methods with synthetic data (semi done)
Tests for derivatives, curvature and torsion are implemented using synthetic data and integrated ABC-Flows.
In one case, the curvature and torsion can be checked to stay constant in the implemented case.

```diff
+ Derivative of synthetic trajectory made of sin and cosin
+ Derivatives of integrated (scipy's odeint) ABC-flow
- ToDo: Curvature and torsion of sin/cos test can be calculated and be tested to be constant.
```

### TODO: Visualize with ParaView (Pyvtk / netcdf) (semi done)
Exporting trajectories for paraview using 'pyvtk' package.
Left to do is exporting a "time series" as a series of files.

### TODO: "TrackJoiner"
Obsolete after inspecting data properly.

```diff
+ Obsolete
```

### TODO: Derivative "mode" with finite difference method (obsolete)
Within testing it came out, that this method is way more off compared to splines.

```diff
+ Obsolete
```

## Installation

1. Create a Python environment with: ```conda create --name tara python=3```.
2. Activate environment with: ```. activate tara```.
3. Install requirements with: ```pip install -r requirements.txt``` (from root folder).
4. Install this package with: ```python setup.py develop```.
