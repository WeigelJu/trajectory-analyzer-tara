import pytest
import numpy as np
from numpy import sin, cos
import warnings

from tara.trajectory import Trajectory, TrajectorySet
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint

def test_derivative_sinus_cosinus():
    # TODO: Torsion / curvature is constant in this case.

    # Parameters for spiral trajectory
    omega = 0.042*np.pi
    t = 100
    dt = 1
    dz=.1
    Z = 10

    T = np.arange(0, t, dt)
    z = np.arange(0, Z, .1)
    x = sin(omega*T)
    y = -cos(omega*T)
    dx = omega*cos(omega*T)
    dy = omega*sin(omega*T)

    # Creating Trajectory object
    test_trajectory = Trajectory(np.array([x, y, z]), T)
    test_trajectory.calc_torsion()

    # Calculating errors from analytical values
    error_x = np.array((test_trajectory._pos_dot_1[0] - dx)**2).mean()
    error_y = np.array((test_trajectory._pos_dot_1[1] - dy)**2).mean()

    if error_x > 1e-10 or error_y > 1e-10:
        warnings.warn(f'Warning: derivative of sinus and cosinus are relatively big. x_err: {error_x}, y_err: {error_y}')

    assert error_x <= 1e-10
    assert error_y <= 1e-10

def test_ABC_curvature():

    # Parameters
    A = 1.1112
    B = 2.0931
    C = 3.1946

    start = [2.112, -0.01, 9.011, -1.124, 0.1, -2.0124]
    x0, y0, z0, xd0, yd0, zd0 = start

    # Differential Equation for ABC flow
    def dr_dt(r, t):

        dr0 = r[3]
        dr1 = r[4]
        dr2 = r[5]
        dr3 = A * sin(dr2) + C * cos(dr1)
        dr4 = B * sin(dr0) + A * cos(dr2)
        dr5 = C * sin(dr1) + B * cos(dr0)
        return [dr0, dr1, dr2, dr3, dr4, dr5]

    # dt : timestep for integration
    dt = 0.02
    t = np.arange(0, 35, dt)
    integrated_solution = odeint(dr_dt, start, t)

    # Creating Trajectory object with integrated positions & calculating curvature
    ABC_trajectory = Trajectory(np.array([integrated_solution[:,0], integrated_solution[:,1], integrated_solution[:,2]]), t)
    ABC_trajectory.calc_torsion()

    # Preparing another Trajectory object with first derivatives from integrated solution
    ABC_trajectory_2 = Trajectory(np.array([integrated_solution[:,0], integrated_solution[:,1], integrated_solution[:,2]]), t)
    ABC_trajectory_2._pos_dot_1 = np.array([integrated_solution[:,3], integrated_solution[:,4], integrated_solution[:,5]])
    ABC_trajectory_2.calc_curvature()

    # Calculating differences
    error_xdot = np.array((ABC_trajectory._pos_dot_1[0] - integrated_solution[:,3])**2).mean()
    error_ydot = np.array((ABC_trajectory._pos_dot_1[1] - integrated_solution[:,4])**2).mean()
    error_zdot = np.array((ABC_trajectory._pos_dot_1[2] - integrated_solution[:,5])**2).mean()
    error_curv = np.array((ABC_trajectory.curvature - ABC_trajectory_2.curvature)**2).mean()

    assert error_xdot <= 1e-12
    assert error_ydot <= 1e-12
    assert error_zdot <= 1e-12
    assert error_curv <= 1e-12
