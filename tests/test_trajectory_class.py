import pytest
import numpy as np
from numpy import sin, cos
import warnings
import h5py

from tara.trajectory import Trajectory, TrajectorySet
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint

def test_npz_io():
    """
    Testing npz i.o. by reading importing data from raw file and comparing it to previously calculated and saved data.

    # TODO: Commentetd asserts fail die to major changes in Trajectory and TrajectorySet classes since this test was implemented.
            Create better (syntetic) tests!
    """

    set1 = TrajectorySet()
    set2 = TrajectorySet()

    set1.load_raw('test_tracks.npy', smooth=10)
    set1.calculate_curvature_torsion()

    set2.load_npz('test_tracks_processed.npz')

    print('Comparing trajectories.')

    for index, traj in enumerate(set1.trajectories):

        assert np.allclose(traj.positions, set2.trajectories[index].positions)
        assert np.allclose(traj.time, set2.trajectories[index].time)
        # assert np.allclose(traj.curvature, set2.trajectories[index].curvature)
        # assert np.allclose(traj.torsion, set2.trajectories[index].torsion)
        #
        # assert np.allclose(traj.pos_dot_1, set2.trajectories[index].pos_dot_1)
        # assert np.allclose(traj.pos_dot_2, set2.trajectories[index].pos_dot_2)
        # assert np.allclose(traj.pos_dot_3, set2.trajectories[index].pos_dot_3)

        assert traj._smoothness == set2.trajectories[index]._smoothness

def acceleration_acf_test():
    """
    Testint the auto correlation function for acceleration
    """

    # Creating a Trajectory object with simple data.
    d = np.array([0,1,2,3,4,5,6,7,8,9])
    da = np.array([d,d,d])
    tr = Trajectory(da, d)
    tr.pos_dot_1 = da
    tr.pos_dot_2 = da


    ### Centered ACF ###
    # Testing first, second and last coefficient
    assert tr._acceleration_acf_coefficient(1)[0] == 20
    assert tr._acceleration_acf_coefficient(1)[1] == 20
    assert tr._acceleration_acf_coefficient(1)[2] == 20

    assert tr._acceleration_acf_coefficient(2)[0] == 24
    assert tr._acceleration_acf_coefficient(2)[1] == 24
    assert tr._acceleration_acf_coefficient(2)[2] == 24

    assert tr._acceleration_acf_coefficient(9)[0] == 0
    assert tr._acceleration_acf_coefficient(9)[1] == 0
    assert tr._acceleration_acf_coefficient(9)[2] == 0

    # Testing max index
    assert np.allclose(tr.acceleration_acf_centered(9)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(9)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(9)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))

    # Testing max index +1
    assert np.allclose(tr.acceleration_acf_centered(10)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(10)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(10)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))

    # Testing max index +2
    assert np.allclose(tr.acceleration_acf_centered(11)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(11)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    assert np.allclose(tr.acceleration_acf_centered(11)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))

    # ### Mean ACF ###
    # # Testing max index
    # tr.acceleration_acf_mean(9)[0]
    # assert np.allclose(tr.acceleration_acf_mean(9)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(9)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(9)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    #
    # # Testing max index +1
    # assert np.allclose(tr.acceleration_acf_mean(10)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(10)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(10)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    #
    # # Testing max index +2
    # assert np.allclose(tr.acceleration_acf_mean(11)[0], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(11)[1], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))
    # assert np.allclose(tr.acceleration_acf_mean(11)[2], np.array([25., 20., 24., 18., 21., 14., 16.,  8.,  9.,  0.]))

    # tr.acceleration_acf_centered(-1)

    assert tr.acceleration_acf_centered(-1) is None

    set = TrajectorySet()

    for _ in range(10):
        set.trajectories.append(tr)

    res_x, res_y, res_z = set.acceleration_acf_centered(11)
    res_x

    assert res_x == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
                     [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
                     [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
                     [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
                     [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
                     [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
                     [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
                     [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
                     [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
                     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     []]

    assert res_y == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
                     [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
                     [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
                     [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
                     [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
                     [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
                     [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
                     [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
                     [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
                     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     []]

    assert res_z == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
                     [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
                     [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
                     [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
                     [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
                     [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
                     [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
                     [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
                     [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
                     [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     []]

    # res_x, res_y, res_z = set.acceleration_acf_mean(11)
    # res_x
    #
    # assert res_x == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
    #                  [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
    #                  [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
    #                  [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
    #                  [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
    #                  [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
    #                  [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
    #                  [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
    #                  [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
    #                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #                  []]
    #
    # assert res_y == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
    #                  [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
    #                  [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
    #                  [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
    #                  [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
    #                  [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
    #                  [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
    #                  [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
    #                  [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
    #                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #                  []]
    #
    # assert res_z == [[25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0],
    #                  [20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0],
    #                  [24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0, 24.0],
    #                  [18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0],
    #                  [21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0, 21.0],
    #                  [14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0, 14.0],
    #                  [16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0, 16.0],
    #                  [8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0, 8.0],
    #                  [9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0, 9.0],
    #                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #                  []]
