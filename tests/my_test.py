import numpy as np
import matplotlib.pyplot as plt
import scipy as sci
from scipy import interpolate
from scipy import integrate
from mpl_toolkits.mplot3d import Axes3D

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=18)

plt.close('all')
# data_test = np.load('test_tracks.npy', encoding='bytes', allow_pickle=True)
data0 = np.load('tracks_000052_21ms.npy', encoding='bytes', allow_pickle=True)
# data1 = np.load('tracks_000102.npy', encoding='bytes', allow_pickle=True)


# np.min(data1[:])
# # data0 = np.load('tracks_000102_2.npy', encoding='bytes', allow_pickle=True)
# # data_jonathan = np.load('Jonathan/test_trackfit/test_tracks.npy', encoding='bytes', allow_pickle=True)
#
# lengths = np.zeros(len(data0))
# for i, particle in enumerate(data0):
#     lengths[i] = len(particle[0])
# np.shape(np.where(lengths >= 30))
# np.sort(lengths)

# max_0 = np.argmax(lengths)
#
# p_end = data0[max_0][0][-1], data0[max_0][1][-1], data0[max_0][2][-1]
#
# r = [np.sqrt( (point[0] - p_end[0])**2 + (point[1] - p_end[1])**2 + (point[2] - p_end[2])**2) for point in data1[:,0]]
#
# np.min(r)

# arr = np.zeros(len(data1))
# for i, a in enumerate(data1_min):
#     arr[i] = abs(a[0] - end_x)

fig = plt.figure()
ax = fig.gca(projection='3d')

# for i, particle in enumerate(data0[max_0:max_0+1]):
for i, particle in enumerate(data0[-10100:-10000]):
    # print(i)
    time = particle[4]

    x = particle[0]
    y = particle[1]
    z = particle[2]

    ax.plot(x, y, z)

plt.xlabel('x')
plt.ylabel('y')
plt.show()

# beta = 0.1
#
# xmin = 0
# xmax = 8*np.pi
#
# N1 = 1000
# N2 = 1000
#
# phi = np.linspace(xmin, xmax, N1)
# phi2 = np.linspace(xmin, xmax, N2)
#
# def testfunc(x):
#     return np.sin(x) *  np.exp(- beta * x)
#
# def d4testfunc(x):
#     return (-4 * beta * (-1 + beta**2) * np.cos(x) + ( 1 - 6 *  beta**2 + beta**4) *  np.sin(x))/ np.exp(beta* x)
#
# spl = interpolate.splrep(phi, testfunc(phi), k=5)
# func = interpolate.BSpline(spl[0], spl[1], k=5)
#
# for i in range(4):
#
#     func_v = np.vectorize(func)
#     vals = func_v(phi2)
#
#     spl = interpolate.splrep(phi2, vals, k=5)
#     func = interpolate.BSpline(spl[0], spl[1], k=5).derivative(1)
#
#
# plt.plot(phi2, func(phi2) - d4testfunc(phi2))
#
# spl = interpolate.splrep(phi, testfunc(phi), k=5)
# func = interpolate.BSpline(spl[0], spl[1], k=5)
#
#
# plt.scatter(phi, testfunc(phi))
#
# #phi = np.linspace(0.1,8*np.pi, 1000)
# #plt.plot(phi, func(phi))
# #plt.show()
#
# deri4 = func.derivative(4)
#
# plt.plot(phi2, deri4(phi2) - d4testfunc(phi2))
# plt.show()
#
# i1 = integrate.quad(deri4, 0, 8*np.pi)
# i2 = integrate.quad(d4testfunc, 0, 8*np.pi)
