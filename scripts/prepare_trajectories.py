"""
 please do not delete this header 

 authors: M. Lellep martin.lellep@physik.uni-marburg.de
          M. Linkmann moritz.linkmann@ed.ac.uk

 * reads collection of trajectories in one tracks file
 * calculates curvature and torsion
 * saves individual trajectories as .csv files
 
"""

from pathlib import Path
import argparse
import datetime

import numpy as np
import pandas as pd

from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS


# CLI
parser = argparse.ArgumentParser(description="prepare_trajectories.py - interpolates trajectories from experimental data, calculates curvature/torsion and saves traj. in .csv format")

parser.add_argument("--path",           default = './',      type=str,   help="data file")
parser.add_argument("--unit_space",     default = 'mm',      type=str,   help="units of length")
parser.add_argument("--unit_time",      default = 'ms',      type=str,   help="time units")
parser.add_argument("--smoothness",     default = 0.02,      type=float, help="smoothing parameter in b-spline interpolation")
parser.add_argument("--clip",           default = 0,         type=int,   help="nr points removed from beginning and end of trajectory")
parser.add_argument("--min_length",     default = 500,       type=int,   help="minimum length of trajectory")
parser.add_argument("--stime",          default = 1/1.25e3,  type=float, help="sampling time")

args = parser.parse_args()


# Settings
SMOOTHNESS    = args.smoothness
PATH          = args.path 
#PATH = "/kosydata/shared/Daten-Goettingen/Mixer/0.5Hz_long_series/Run_048/chunk_001/tracks_000001.npy"
CLIP          = args.clip
MIN_LENGTH    = args.min_length
UNIT_SPACE    = args.unit_space
UNIT_TIME     = args.unit_time
SAMPLING_TIME = args.stime

# Create a temporary TrajectorySet object and imports raw data
temp_set = TrS()
temp_set.load_raw(PATH)

# Delete all short trajectories
temp_set.clip = CLIP
temp_set.trajectories = [traj for traj in temp_set.trajectories if traj.len >= MIN_LENGTH]
print('Number of trajectories to choose from:', temp_set.len)
temp_set.smoothness = SMOOTHNESS

temp_set.calculate_curvature_torsion()

trajectory_indices = [1, 5, 10, 4, 19] # test
trajectory_indices = range(temp_set.len) # production

outdir = Path( 'trajectories_data' )
if not outdir.exists():
    outdir.mkdir(parents=True)
data_name = 'trajectory.index{}.csv'
for i in trajectory_indices:
    curvature = temp_set.trajectories[i].curvature
    torsion = temp_set.trajectories[i].torsion
    time = temp_set.trajectories[i].time
    X = temp_set.trajectories[i].X
    Y = temp_set.trajectories[i].Y
    Z = temp_set.trajectories[i].Z
    data_dict = {
            f'time [{UNIT_TIME}]': time*SAMPLING_TIME,
            f'X [{UNIT_SPACE}]': X,
            f'Y [{UNIT_SPACE}]': Y,
            f'Z [{UNIT_SPACE}]': Z,
            'curvature': curvature,
            'torsion': torsion
            }
    df = pd.DataFrame.from_dict(data_dict)
    df.to_csv(outdir / data_name.format(i), index=False)


exit()
