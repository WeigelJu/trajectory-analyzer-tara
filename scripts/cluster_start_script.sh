#!/bin/bash

#$ -S /bin/bash                 # specify the shell to use
#$ -cwd                         # Current workdir
#$ -e /home/lellepgu/stderr     # stderr
#$ -o /home/lellepgu/stdout     # stdout
#$ -N tara                      # job name
#$ -l h_rt=120:00:00            # walltime
#$ -pe orte_smp* 32             # parallel environment
#$ -l h_vmem=4000M              # Memory

# User settings
SMOOTHNESS=0.02
DMODE=full

# Path settings
BASE_PATH=/home/lellepgu/data/goettingen_data/Mixer
INPUT_PATH=${BASE_PATH}/0.5Hz_long_series
OUTPUT_PATH_STEP0=${BASE_PATH}/output/step_0/smooth_${SMOOTHNESS}
OUTPUT_PATH_STEP1=${BASE_PATH}/output/step_1/smooth_${SMOOTHNESS}
OUTPUT_PATH_STEP2=${BASE_PATH}/output/step_2/smooth_${SMOOTHNESS}
OUTPUT_PATH_STEP3=${BASE_PATH}/output/step_3/smooth_${SMOOTHNESS}
OUTPUT_PATH_STEP4=${BASE_PATH}/output/step_4/smooth_${SMOOTHNESS}
OUTPUT_PATH_STEP5=${BASE_PATH}/output/step_5/smooth_${SMOOTHNESS}

mkdir -p ${OUTPUT_PATH_STEP0}
mkdir -p ${OUTPUT_PATH_STEP1}
mkdir -p ${OUTPUT_PATH_STEP2}
mkdir -p ${OUTPUT_PATH_STEP3}
mkdir -p ${OUTPUT_PATH_STEP4}
mkdir -p ${OUTPUT_PATH_STEP5}

# Python settings
PYTHON=/home/lellepgu/anaconda3/envs/tara/bin/python

# ===================================================================================
# Step 0
# ===================================================================================

${PYTHON} 0_preprocess.py -i ${INPUT_PATH} -o ${OUTPUT_PATH_STEP0} -dmode ${DMODE} -smooth ${SMOOTHNESS} -s 41 -min 70 -p ${NSLOTS}

echo Step 0 error code: $?

# ===================================================================================
# Step 1
# ===================================================================================

${PYTHON} 1_calculate_rms_mean_parallel.py -i ${OUTPUT_PATH_STEP0} -o ${OUTPUT_PATH_STEP1} -dmode ${DMODE} -p ${NSLOTS}

echo Step 1 error code: $?

# ===================================================================================
# Step 2
# ===================================================================================

${PYTHON} 2_conditional_statistics.py -i ${OUTPUT_PATH_STEP0} -irms ${OUTPUT_PATH_STEP1} -o ${OUTPUT_PATH_STEP2} -dmode ${DMODE} -p ${NSLOTS}

echo Step 2 error code: $?

# ===================================================================================
# Step 3
# ===================================================================================

${PYTHON} 3_acceleration_acf.py -i ${OUTPUT_PATH_STEP2} -o ${OUTPUT_PATH_STEP3}

echo Step 3 error code: $?

# ===================================================================================
# Step 4
# ===================================================================================

${PYTHON} 4_sum_histogramms.py -i ${OUTPUT_PATH_STEP2} -irms ${OUTPUT_PATH_STEP1} -o ${OUTPUT_PATH_STEP4}

echo Step 4 error code: $?

# ===================================================================================
# Step 5
# ===================================================================================

${PYTHON} 5_plot_histogramms.py -i ${OUTPUT_PATH_STEP4} -irms ${OUTPUT_PATH_STEP1} -iacf ${OUTPUT_PATH_STEP3} -o ${OUTPUT_PATH_STEP5}

echo Step 5 error code: $?

echo
echo Done
