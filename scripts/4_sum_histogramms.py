from tara.trajectory import *
from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.visualize import *
import numpy as np
import matplotlib.pyplot as plt
import scipy as sci
import scipy.signal as sig
import os
import argparse

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=18)

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-irms', '--input-rms', type=str, metavar='', required=True, help='Input folder rms files.')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for hist files.')
args = parser.parse_args()
# =============================================================================


filelist = list()
# rootdir = 'D:/raw_to_npz'

for subdir, dirs, files in os.walk(args.input):
    for file in files:
        filelist.append((file, os.path.join(subdir, file)))

alpha_files = [file for file in filelist if 'hist_alpha_' in file[0]]
velocity_files = [file for file in filelist if 'hist_veloc_' in file[0]]
acceleration_files = [file for file in filelist if 'hist_accel_' in file[0]]
const_acceleration_files = [file for file in filelist if 'hist_cond_accel_' in file[0]]
# delu_files = [file for file in filelist if 'hist_delu_' in file[0]]

# =============================================================================
# Parameters
alpha_bin_centers = [0.01, 0.05, 0.1, 0.5, 1, 2, 4, 6, 8, 10, 12, 16, 20, 24, 28, 32, 48, 64]
rel_error = 0.1
tau_steps = [5, 10, 18, 36, 72, 136, 286]
n_hist_bins = 501

# Mean alpha and RMS calculated with script #1
mean_alpha = np.load(f'{args.input_rms}/mean_alpha.npy')[0]
rms_velocity = np.load(f'{args.input_rms}/rms_veloc.npy')[0]
rms_acceleration = np.load(f'{args.input_rms}/rms_accel.npy')[0]

# Global fields for histogramm data
bins_hist_accelerations = rms_acceleration*np.linspace(-60, 60, 2*n_hist_bins -1)
bins_hist_velocity = rms_velocity*np.linspace(-10, 10, n_hist_bins)
bins_hist_alpha = mean_alpha*np.logspace(np.log10(1e-3), np.log10(1e3), 300)
bins_hist_delu = rms_velocity*np.linspace(-60, 60, n_hist_bins)

hist_accelerations_x, _ = np.histogram([], bins=bins_hist_accelerations)
hist_accelerations_y, _ = np.histogram([], bins=bins_hist_accelerations)
hist_accelerations_z, _ = np.histogram([], bins=bins_hist_accelerations)
hist_velocity_x, _ = np.histogram([], bins=bins_hist_velocity)
hist_velocity_y, _ = np.histogram([], bins=bins_hist_velocity)
hist_velocity_z, _ = np.histogram([], bins=bins_hist_velocity)

hist_alpha, _ = np.histogram([], bins=bins_hist_alpha)

cond_hist_accel_x = [np.histogram([], bins=bins_hist_accelerations)[0] for _ in alpha_bin_centers]
cond_hist_accel_y = [np.histogram([], bins=bins_hist_accelerations)[0] for _ in alpha_bin_centers]
cond_hist_accel_z = [np.histogram([], bins=bins_hist_accelerations)[0] for _ in alpha_bin_centers]

# hist_del_ux = [np.histogram([], bins=bins_hist_delu)[0] for _ in tau_steps]
# hist_del_uy = [np.histogram([], bins=bins_hist_delu)[0] for _ in tau_steps]
# hist_del_uz = [np.histogram([], bins=bins_hist_delu)[0] for _ in tau_steps]

# =============================================================================

print(f'Step 3: Summing up histogramms.')

for i, file in enumerate(acceleration_files):
    hist_accelerations_x += np.load(file[1], allow_pickle=True)[0]
    hist_accelerations_y += np.load(file[1], allow_pickle=True)[1]
    hist_accelerations_z += np.load(file[1], allow_pickle=True)[2]

for i, file in enumerate(velocity_files):
    hist_velocity_x += np.load(file[1], allow_pickle=True)[0]
    hist_velocity_y += np.load(file[1], allow_pickle=True)[1]
    hist_velocity_z += np.load(file[1], allow_pickle=True)[2]


for i, file in enumerate(alpha_files):
    hist_alpha += np.load(file[1], allow_pickle=True)

for i, file in enumerate(const_acceleration_files):
    cond_hist_accel_x += np.load(file[1], allow_pickle=True)[0]
    cond_hist_accel_y += np.load(file[1], allow_pickle=True)[1]
    cond_hist_accel_z += np.load(file[1], allow_pickle=True)[2]

# for i, file in enumerate(delu_files):
#     del_ux = np.load(file[1], allow_pickle=True)[0]
#     del_uy = np.load(file[1], allow_pickle=True)[1]
#     del_uz = np.load(file[1], allow_pickle=True)[2]
#     for j, _ in enumerate(tau_steps):
#         hist_del_ux[j] += del_ux[j]
#         hist_del_uy[j] += del_uy[j]
#         hist_del_uz[j] += del_uz[j]


np.save(f'{args.output}/hist_accelerations_x', hist_accelerations_x)
np.save(f'{args.output}/hist_accelerations_y', hist_accelerations_y)
np.save(f'{args.output}/hist_accelerations_z', hist_accelerations_z)

np.save(f'{args.output}/hist_velocity_x', hist_velocity_x)
np.save(f'{args.output}/hist_velocity_y', hist_velocity_y)
np.save(f'{args.output}/hist_velocity_z', hist_velocity_z)

np.save(f'{args.output}/hist_alpha', hist_alpha)

np.save(f'{args.output}/cond_hist_accel_x', cond_hist_accel_x)
np.save(f'{args.output}/cond_hist_accel_y', cond_hist_accel_y)
np.save(f'{args.output}/cond_hist_accel_z', cond_hist_accel_z)

# np.save(f'{args.output}/hist_delu_x', hist_del_ux)
# np.save(f'{args.output}/hist_delu_y', hist_del_uy)
# np.save(f'{args.output}/hist_delu_z', hist_del_uz)
