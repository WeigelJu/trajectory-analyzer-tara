Run the command ```snakemake -R all --cores 4``` to compute the
energy vs time on 4 cores (--cores) no matter if previous
results have been already obtained (-R).

Alternatively, use "all" instead of "energy_vs_time".

This snakemake script needs to be modified to accommodate your
E-vs-t script logic properly.
