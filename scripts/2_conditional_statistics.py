from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.progressbar import printProgressBar
import matplotlib.pyplot as plt

import numpy as np
import concurrent.futures

import time
from tqdm import tqdm
import os
import argparse

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-irms', '--input-rms', type=str, metavar='', required=True, help='Input folder rms files.')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for hist files.')
parser.add_argument('-dmode', '--data-mode', type=str, default='full', metavar='', help='Data mode says how much of the data will be processed: microtest, test, chunk or run or full (default is full data).')
parser.add_argument('-p', '--processes', type=int, default=1, metavar='', help='Number of files process parallel. For optimal performance set this to (real) CPU count.')
args = parser.parse_args()
# =============================================================================


# =============================================================================
# Parameters
alpha_bin_centers = [0.01, 0.05, 0.1, 0.5, 1, 2, 4, 6, 8, 10, 12, 16, 20, 24, 28, 32, 48, 64]
tau_steps = [5, 10, 18, 36, 72, 136, 286]
rel_error = 0.1

n_hist_bins = 501

# Mean alpha and RMS calculated with script #1
mean_alpha = np.load(f'{args.input_rms}/mean_alpha.npy')[0]
rms_velocity = np.load(f'{args.input_rms}/rms_veloc.npy')[0]
rms_acceleration = np.load(f'{args.input_rms}/rms_accel.npy')[0]
cond_accel_rms = np.load(f'{args.input_rms}/rms_cond.npy')[0]

# Global fields for histogramm data
bins_hist_accelerations = rms_acceleration*np.linspace(-60, 60, 2*n_hist_bins -1)
bins_hist_velocity = rms_velocity*np.linspace(-10, 10, n_hist_bins)
bins_hist_alpha = mean_alpha*np.logspace(np.log10(1e-3), np.log10(1e3), 300)
# bins_hist_delu = rms_velocity*np.linspace(-60, 60, n_hist_bins)
# =============================================================================

# =============================================================================
# Method to map alpha value to indices arrays for conditional statistic
def alpha_index(alpha, mean_alpha, bin_centers, relative_error):

    for index, bc in enumerate(bin_centers):

        if alpha >= mean_alpha*bc*(1 - relative_error) and alpha <= mean_alpha*bc*(1 + relative_error):
            return index

    return -1
# =============================================================================

# =============================================================================
# Creating file list
filelist = list()
for subdir, dir, files in os.walk(args.input):
    for file in files:
        filelist.append((file, subdir))

# Remove all not following the naming convention
filelist = [file for file in filelist if file[0].startswith('Run_') and file[0].endswith('.npz')]

if args.data_mode == 'microtest':
    filelist = filelist[:8]
if args.data_mode == 'test':
    filelist = filelist[:24]
if args.data_mode == 'chunk':
    filelist = [file for file in filelist if "chunk_001" in file[0] and "Run_045" in file[0]]
if args.data_mode == 'twochunks':
    filelist = [file for file in filelist if ("chunk_001" in file[0] or "chunk_002" in file[0]) and "Run_046" in file[0]]
if args.data_mode == 'run':
    filelist = [file for file in filelist if "Run_046" in file[0]]
if args.data_mode == 'full':
    filelist = filelist
# =============================================================================

def data_extractor(file):

    # Creating set and loadding file.
    set = TrS()
    set.load_npz(os.path.join(file[1], file[0]))
    set.clip = 14

    # Importing globals
    global mean_alpha
    global bins_hist_accelerations
    global bins_hist_velocity
    global bins_hist_alpha
    global tau_steps

    # Containers for conditional acceleration and velocity
    hist_alpha, _ = np.histogram([], bins=bins_hist_alpha)
    hist_veloc_x, _ = np.histogram([], bins=bins_hist_velocity)
    hist_veloc_y, _ = np.histogram([], bins=bins_hist_velocity)
    hist_veloc_z, _ = np.histogram([], bins=bins_hist_velocity)
    hist_accel_x, _ = np.histogram([], bins=bins_hist_accelerations)
    hist_accel_y, _ = np.histogram([], bins=bins_hist_accelerations)
    hist_accel_z, _ = np.histogram([], bins=bins_hist_accelerations)

    # Bins for conditional statisrics have to be rescaled with RMS of the bin.
    cond_hist_accel_x = [np.histogram([], bins=np.linspace(-60, 60, 2*n_hist_bins -1) * cond_accel_rms[i])[0] for i, _ in enumerate(alpha_bin_centers)]
    cond_hist_accel_y = [np.histogram([], bins=np.linspace(-60, 60, 2*n_hist_bins -1) * cond_accel_rms[i])[0] for i, _ in enumerate(alpha_bin_centers)]
    cond_hist_accel_z = [np.histogram([], bins=np.linspace(-60, 60, 2*n_hist_bins -1) * cond_accel_rms[i])[0] for i, _ in enumerate(alpha_bin_centers)]

    # del_ux = [list() for _ in tau_steps]
    # del_uy = [list() for _ in tau_steps]
    # del_uz = [list() for _ in tau_steps]

    # Velocity increments

    # Loops over each trajectory
    for traj in set.trajectories:

        v = traj.pos_dot_1
        a = traj.pos_dot_2

        # Container for conditional acceleration statitic acceleration values
        container_cond_accel_x = [list() for _ in alpha_bin_centers]
        container_cond_accel_y = [list() for _ in alpha_bin_centers]
        container_cond_accel_z = [list() for _ in alpha_bin_centers]

        # Conditioning acceleration on alpha values
        for index, alpha_i in enumerate(traj.cgl_acceleration):

            bin_index = alpha_index(alpha_i, mean_alpha, alpha_bin_centers, rel_error)

            if bin_index != -1:
                container_cond_accel_x[bin_index].append(a[0][index])
                container_cond_accel_y[bin_index].append(a[1][index])
                container_cond_accel_z[bin_index].append(a[2][index])

        # Adding histogramm of single trajectory to the ones for the enire set
        hist_alpha += np.histogram(traj.cgl_acceleration, bins=bins_hist_alpha)[0]
        hist_veloc_x += np.histogram(v[0], bins=bins_hist_velocity)[0]
        hist_veloc_y += np.histogram(v[1], bins=bins_hist_velocity)[0]
        hist_veloc_z += np.histogram(v[2], bins=bins_hist_velocity)[0]
        hist_accel_x += np.histogram(a[0], bins=bins_hist_accelerations)[0]
        hist_accel_y += np.histogram(a[1], bins=bins_hist_accelerations)[0]
        hist_accel_z += np.histogram(a[2], bins=bins_hist_accelerations)[0]

        # Creating histogramm for conditioned accelerations
        for index, _ in enumerate(alpha_bin_centers):
            cond_hist_accel_x[index] += np.histogram(container_cond_accel_x[index] , bins=bins_hist_accelerations / rms_acceleration * cond_accel_rms[index])[0]
            cond_hist_accel_y[index] += np.histogram(container_cond_accel_y[index] , bins=bins_hist_accelerations / rms_acceleration * cond_accel_rms[index])[0]
            cond_hist_accel_z[index] += np.histogram(container_cond_accel_z[index], bins=bins_hist_accelerations / rms_acceleration * cond_accel_rms[index])[0]

    # To reduce calculation efford set is reduced to trajectories of length >= 10 tau_eta + 2*clip
    # Correlation functions are calculated afterwards
    set.trajectories = [traj for traj in set.trajectories if len(traj.time) >= 136 + 2*set.trajectories[0].clip]
    acf_x, acf_y, acf_z = set.acceleration_acf_mean(tau_max = 136)

    # for i, tau in enumerate(tau_steps):
    #     del_u = set.velocity_increment(tau)
    #
    #     del_ux[i].extend(del_ux[0])
    #     del_uy[i].extend(del_uy[1])
    #     del_uz[i].extend(del_uz[2])

    # Writing all to files
    np.save(f'{args.output}/hist_alpha_{file[0][:-4]}', hist_alpha)
    np.save(f'{args.output}/hist_veloc_{file[0][:-4]}', np.array([hist_veloc_x, hist_veloc_y, hist_veloc_z]))
    np.save(f'{args.output}/hist_accel_{file[0][:-4]}', np.array([hist_accel_x, hist_accel_y, hist_accel_z]))
    np.save(f'{args.output}/hist_cond_accel_{file[0][:-4]}', np.array([cond_hist_accel_x, cond_hist_accel_y, cond_hist_accel_z]))
    np.save(f'{args.output}/acf_{file[0][:-4]}', np.array([acf_x, acf_y, acf_z]))
    # np.save(f'{args.output}/hist_delu_{file[0][:-4]}', np.asarray([del_ux, del_uy, del_uz]))

    return 0

# Main Method feeding files to processes
if __name__ == '__main__':

    print(f'Processing conditional statistic.')
    print(f'<alpha> = {mean_alpha}')
    print(f'<v^2> = {rms_velocity}')
    print(f'<a^2> = {rms_acceleration}')

    with concurrent.futures.ProcessPoolExecutor(max_workers=args.processes) as executor:
        results = list(tqdm(executor.map(data_extractor, filelist), total=len(filelist)))
    print('\n')
