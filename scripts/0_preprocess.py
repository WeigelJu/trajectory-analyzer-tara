from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.progressbar import printProgressBar

import numpy as np
from concurrent.futures import ProcessPoolExecutor
import os
import argparse

import time
from tqdm import tqdm

# =============================================================================
# Settung up argument parser
parser = argparse.ArgumentParser(description='Ready a numpy file from Goettingen and creates a hdf5 with processed data.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with files from Goettingen. This folder is searched recursivly.')
parser.add_argument('-o', '--output', type=str, metavar='', default='', help='Output folder for npz files. Output files will foollow naming convention run_xx_chunk_yy_tracks_zzzzzz.npz')
parser.add_argument('-s', '--sigma', type=int, default=0, metavar='', help='If set other than 0, coarse grain lagrangian acceleration will be calculated with input as sigma.')
parser.add_argument('-p', '--processes', type=int, default=1, metavar='', help='Number of files processec parallel. For optimal performance set this to (real) CPU count.')
parser.add_argument('-c', '--curvature', type=bool, metavar='', default=False, help='If set True, curvature will be calculated (default=False).')
parser.add_argument('-t', '--torsion', type=bool, metavar='', default=False, help='If set True, torsion and curvature will be calculated (default=False).')
parser.add_argument('-min', '--min-length', type=int, default=70, metavar='', help='Minimum length of trajectories saved in hdf5 file (default=10).')
parser.add_argument('-deriv', '--derivative', type=int, default=2, metavar='', help='Minimum order of derivative to calculate (default=2).')
parser.add_argument('-smooth', '--smoothness', type=float, default=0.003, metavar='', help='Smoothness parameter (default=0.003).')
parser.add_argument('-dmode', '--data-mode', type=str, default='full', metavar='', help='Data mode says how much of the data will be processed: microtest, test, chunk or run or full (default is full data).')

args = parser.parse_args()
# =============================================================================

# =============================================================================
### Setting the default output file name ###
output_folder = args.output
if output_folder == '':
    output_folder = args.input
# =============================================================================

# =============================================================================
### Creating a list of all files in given folder ###

filelist = list()
for subdir, dirs, files in os.walk(args.input):
    for file in files:
        filelist.append((file, os.path.join(subdir, file)))

# Remove those files from list that do not follow naming convention for raw data.
filelist = [file for file in filelist if file[0].endswith('.npy') and file[0].startswith('tracks_')]

if args.data_mode == 'microtest':
    filelist = filelist[:8]
if args.data_mode == 'test':
    filelist = filelist[:24]
if args.data_mode == 'chunk':
    filelist = [file for file in filelist if "chunk_001" in file[1] and "Run_045" in file[1]]
if args.data_mode == 'twochunks':
    filelist = [file for file in filelist if (("chunk_001" in file[1] or "chunk_002" in file[0]) and "Run_046" in file[1])]
if args.data_mode == 'run':
    filelist = [file for file in filelist if "Run_046" in file[1]]
if args.data_mode == 'full':
    filelist = filelist
# =============================================================================

# =============================================================================
### Method to multiprocess ###
def worker(file):

    sigma = args.sigma
    curvature = args.curvature
    torsion = args.torsion
    min_length = args.min_length
    derivative = args.derivative
    smoothness = args.smoothness

    try:
        # Create a temporary TrajectorySet object and imports raw data
        temp_set = TrS()
        temp_set.load_raw(file[1])
        temp_set.verbose = False

        # Delete all short trajectories
        temp_set.trajectories = [traj for traj in temp_set.trajectories if len(traj.time) >= args.min_length]
        temp_set.smoothness = smoothness

        ### Create name string for output file
        # Replacing '\\' with '/'. Only matters on Windows machines!
        path = file[1].replace('\\', '/')

        run_string = path.split('/')[-3]
        chunk_string = path.split('/')[-2]
        filename_string = file[0][:-4]

        out_file = f'{run_string}_{chunk_string}_{filename_string}'

        # Check if TrajectorySet.trajectories is not emptry!
        # Then calculate cgl_acceleration, torsion, curvature according to passed arguments
        if temp_set.trajectories:
            if args.sigma > 0:
                temp_set.calc_cgl_acceleration(sigma)
            if args.curvature:
                temp_set.calc_curvature()
            if args.torsion:
                temp_set.calculate_curvature_torsion()
            if args.derivative > 0:
                temp_set.calc_derivative(order = derivative)

        # Write out file
        temp_set.save_npz(f'{output_folder}/{out_file}.npz')
        return 0

    except:
        print(f'Error while converting {file[1]}.')
        return -1
# =============================================================================

# =============================================================================
### Feed Pool with worker jobs ###
if __name__ == '__main__':
    # Print after initialize
    print('Running raw_to_npz script to converte raw data from Goettingen to npz files. \n')


    with ProcessPoolExecutor(max_workers=args.processes) as executor:

        sigma = args.sigma
        curvature = args.curvature
        torsion = args.torsion
        min_length = args.min_length
        derivative = args.derivative
        smoothness = args.smoothness

        result = list(tqdm(executor.map(worker, filelist), total=len(filelist)))

    print('\n')
# =============================================================================
