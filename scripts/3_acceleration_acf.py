from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.progressbar import printProgressBar
import matplotlib.pyplot as plt

import numpy as np
import concurrent.futures

import os
import argparse

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=18)

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for hist files.')
args = parser.parse_args()
# =============================================================================
#
# =============================================================================
# Creating file list
# root_dir = 'D:/output/step_2/smooth_0.003'
# root_dir = 'C:/Users/joule/Nextcloud/Vorlesungen/BA/trajectory-analyzer-tara/tara/temp_files/'
filelist = list()
for subdir, dir, files in os.walk(args.input):
# for subdir, dir, files in os.walk(root_dir):
    for file in files:
        filelist.append((file, os.path.join(subdir, file)))

# Remove all not following the naming convention
filelist = [file for file in filelist if file[0].startswith('acf_') and file[0].endswith('.npy')]
# =============================================================================

# =============================================================================
# Fields and Parameters

# TODO: Add parsable argument for tau_max
tau_max = 136

acf_values_means_x = np.zeros(tau_max)
acf_values_means_y = np.zeros(tau_max)
acf_values_means_z = np.zeros(tau_max)

acf_values_blocks_x = [list() for tau in range(tau_max)]
acf_values_blocks_y = [list() for tau in range(tau_max)]
acf_values_blocks_z = [list() for tau in range(tau_max)]

acf_values_num_blocks_x = np.zeros(tau_max)
acf_values_num_blocks_y = np.zeros(tau_max)
acf_values_num_blocks_z = np.zeros(tau_max)

for file_index, file in enumerate(filelist):

    blocksize = 10000
    vals_x, vals_y, vals_z = np.load(file[1], allow_pickle=True)

    for tau in range(tau_max):
        acf_values_blocks_x[tau].extend(vals_x[tau])
        acf_values_blocks_y[tau].extend(vals_y[tau])
        acf_values_blocks_z[tau].extend(vals_z[tau])

    for tau, block in enumerate(acf_values_blocks_x):
        if len(block) >= blocksize:
            acf_values_means_x[tau] += np.mean(block[:blocksize])
            del block[:blocksize]
            acf_values_num_blocks_x[tau] += 1

    for tau, block in enumerate(acf_values_blocks_y):
        if len(block) >= blocksize:
            acf_values_means_y[tau] += np.mean(block[:blocksize])
            del block[:blocksize]
            acf_values_num_blocks_y[tau] += 1

    for tau, block in enumerate(acf_values_blocks_z):
        if len(block) >= blocksize:
            acf_values_means_z[tau] += np.mean(block[:blocksize])
            del block[:blocksize]
            acf_values_num_blocks_z[tau] += 1

    printProgressBar(file_index, len(filelist), prefix=f'Combining ACF-Files:', suffix=f'{file_index+1} of {len(filelist)} done.', length=50)

acf_values_means_x /= acf_values_num_blocks_x
acf_values_means_y /= acf_values_num_blocks_y
acf_values_means_z /= acf_values_num_blocks_z

acf_values_means_x *= acf_values_means_x[0]
acf_values_means_y *= acf_values_means_y[0]
acf_values_means_z *= acf_values_means_z[0]

np.save(f'{args.output}/acf_combined.npy', np.array([acf_values_means_x, acf_values_means_y, acf_values_means_z]))





# ===========================================================
