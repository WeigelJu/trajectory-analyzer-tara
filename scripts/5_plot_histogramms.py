from tara.trajectory import *
from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.visualize import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.gridspec import GridSpec
import scipy as sci
import scipy.signal as sig
import os
import argparse

plt.rc('font', family='serif')
plt.rc('font', size=14)
mpl.rcParams['axes.linewidth'] = 1 #set the value globally
mpl.rcParams['lines.linewidth'] = 3

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-irms', '--input-rms', type=str, metavar='', required=True, help='Input folder rms files.')
parser.add_argument('-iacf', '--input-acf', type=str, metavar='', required=True, help='Input folder ACF files.')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for hist files.')
args = parser.parse_args()
# =============================================================================

# cond_accel_rms = [0, 0, 0, 0.02478033, 0.03539765, 0.05588228, 0.0832307, 0.11032523, 0.12036485, 0.16967252, 0, 0, 0, 0, 0, 0, 0]


### Note
# PDF is numerically not normalized but the plot is. This is due to the "bins" are given in units of rms
# while the data have not been calculated with theese bins. So there is a factor proportional to the
# bin width...

# =============================================================================
# Parameters

# rootdir = 'D:/raw_to_npz_0.03_smooth/hist'
rootdir = 'D:/output/step_3/smooth_0.003'
# rootdir = 'D:/0.5Hz_long_series__output_cluster/step_3/smooth_0.003'

cond_accel_rms = np.load(f'{args.input_rms}/rms_cond.npy')[0]
alpha_bin_centers = [0.01, 0.05, 0.1, 0.5, 1, 2, 4, 6, 8, 10, 12, 16, 20, 24, 28, 32, 48, 64]
tau_steps = [5, 10, 18, 36, 72, 136, 286]
n_hist_bins = 501
colors = ['gold', 'yellowgreen', 'seagreen', 'steelblue', 'darkslateblue', 'purple']


rel_error = 0.1

# Mean alpha and RMS calculated with script #1
mean_alpha = np.load(f'{args.input_rms}/mean_alpha.npy')[0]
rms_velocity = np.load(f'{args.input_rms}/rms_veloc.npy')[0]
rms_acceleration = np.load(f'{args.input_rms}/rms_accel.npy')[0]
flatness_acceleration = np.load(f'{args.input_rms}/flatness_cond.npy')[0]
# mean_energy = np.load('D:/output/energy/mean_energy.npy')
# std_energy = np.load('D:/output/energy/std_energy.npy')

# Global fields for histogramm data
bins_hist_accelerations = np.linspace(-60, 60, 2*n_hist_bins -2)
bins_hist_velocity = np.linspace(-10, 10, n_hist_bins -1)
bins_hist_alpha = np.logspace(np.log10(1e-3), np.log10(1e3), 300 -1)
bins_hist_delu = np.linspace(-60, 60, n_hist_bins)

b_alpha = np.logspace(np.log10(1e-3), np.log10(1e3), 300)
bins_hist_alpha_widths =  np.asarray([b_alpha[i+1] - b_alpha[i] for i in range(299)])

# Loading
hist_alpha = np.load(f'{args.input}/hist_alpha.npy').astype(float)

hist_accelerations_x = np.load(f'{args.input}/hist_accelerations_x.npy').astype(float)
hist_accelerations_y = np.load(f'{args.input}/hist_accelerations_y.npy').astype(float)
hist_accelerations_z = np.load(f'{args.input}/hist_accelerations_z.npy').astype(float)

hist_velocity_x = np.load(f'{args.input}/hist_velocity_x.npy').astype(float)
hist_velocity_y = np.load(f'{args.input}/hist_velocity_y.npy').astype(float)
hist_velocity_z = np.load(f'{args.input}/hist_velocity_z.npy').astype(float)

cond_hist_accel_x = np.load(f'{args.input}/cond_hist_accel_x.npy').astype(float)
cond_hist_accel_y = np.load(f'{args.input}/cond_hist_accel_y.npy').astype(float)
cond_hist_accel_z = np.load(f'{args.input}/cond_hist_accel_z.npy').astype(float)

hist_accelerations = hist_accelerations_x + hist_accelerations_y + hist_accelerations_z
hist_velocity =  hist_velocity_x + hist_velocity_y + hist_velocity_z
cond_hist_accel = cond_hist_accel_x + cond_hist_accel_y + cond_hist_accel_z

acf_x = np.load(f'{args.input_acf}/acf_combined.npy', allow_pickle=True)[0]
acf_y = np.load(f'{args.input_acf}/acf_combined.npy', allow_pickle=True)[1]
acf_z = np.load(f'{args.input_acf}/acf_combined.npy', allow_pickle=True)[2]

# del_ux = np.load(f'D:/output/step_3/smooth_0.003/hist_delu_x.npy')
# del_uy = np.load(f'D:/output/step_3/smooth_0.003/hist_delu_y.npy')
# del_uz = np.load(f'D:/output/step_3/smooth_0.003/hist_delu_z.npy')

# Normalization
hist_velocity_x = hist_velocity_x / np.sum(hist_velocity_x) / (bins_hist_velocity[1] - bins_hist_velocity[0])
hist_velocity_y = hist_velocity_y / np.sum(hist_velocity_y) / (bins_hist_velocity[1] - bins_hist_velocity[0])
hist_velocity_z = hist_velocity_z / np.sum(hist_velocity_z) / (bins_hist_velocity[1] - bins_hist_velocity[0])

hist_accelerations_x = hist_accelerations_x / np.sum(hist_accelerations_x) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
hist_accelerations_y = hist_accelerations_y / np.sum(hist_accelerations_y) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
hist_accelerations_z = hist_accelerations_z / np.sum(hist_accelerations_z) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])

for i in range(len(alpha_bin_centers)):
    cond_hist_accel_x[i] = cond_hist_accel_x[i] / np.sum(cond_hist_accel_x[i]) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
    cond_hist_accel_y[i] = cond_hist_accel_y[i] / np.sum(cond_hist_accel_y[i]) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
    cond_hist_accel_z[i] = cond_hist_accel_z[i] / np.sum(cond_hist_accel_z[i]) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
    cond_hist_accel[i] = cond_hist_accel[i] / np.sum(cond_hist_accel[i]) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])

hist_accelerations = hist_accelerations / np.sum(hist_accelerations) / (bins_hist_accelerations[1] - bins_hist_accelerations[0])
hist_velocity = hist_velocity / np.sum(hist_velocity) / (bins_hist_velocity[1] - bins_hist_velocity[0])

hist_alpha = hist_alpha / np.sum(hist_alpha) / bins_hist_alpha_widths

# for i, _ in enumerate(tau_steps):
#     del_ux[i] = del_ux[i] / np.sum(del_ux[i]) / (bins_hist_delu[1] - bins_hist_delu[0])
#     del_uy[i] = del_uy[i] / np.sum(del_uy[i]) / (bins_hist_delu[1] - bins_hist_delu[0])
#     del_uz[i] = del_uz[i] / np.sum(del_uz[i]) / (bins_hist_delu[1] - bins_hist_delu[0])
# =============================================================================

# =============================================================================
# alpha

for i, i_bin in enumerate([0, 2, 4, 6, 11, 17]):
    alpha_bin_center_value = alpha_bin_centers[i_bin]
    a_bin_index = (np.abs(alpha_bin_center_value-bins_hist_alpha)).argmin()
    plt.plot([bins_hist_alpha[a_bin_index], bins_hist_alpha[a_bin_index]],[0, hist_alpha[a_bin_index]], color=colors[i])

plt.plot(bins_hist_alpha, hist_alpha, c='r')
plt.xlabel(r'$\alpha / \langle \alpha \rangle$')
plt.ylabel(r'$\langle \alpha \rangle f(\alpha)$')
plt.xticks([1e-3, 1e-2, 1e-1, 1e-0, 1e-1, 1e-2, 1e-3])
plt.xscale('log')
plt.yscale('log')
plt.savefig(f'{args.output}/pdf_alpha_full.pdf')
# plt.show()
# =============================================================================

# =============================================================================
# Plot of del_u

# plt.errorbar(range(len(mean_energy)), mean_energy / np.nanmean(mean_energy), yerr=std_energy/np.nanmean(mean_energy), c='thistle', alpha=0.2, label='Standardabweichung')
# plt.plot(np.linspace(-60,60,500), del_uy[3], c='darkmagenta', label=r'del_ux')
# # plt.xlabel(r'$t / \tau_\eta$')
# # plt.ylabel(r'$\langle E(t)\rangle_V / \langle E \rangle_{V, \tau_\eta}$'),
# plt.legend(frameon=False)
# plt.yscale('log')
# plt.show()
# =============================================================================

# =============================================================================
# Plot of Energy

# plt.errorbar(range(len(mean_energy)), mean_energy / np.nanmean(mean_energy), yerr=std_energy/np.nanmean(mean_energy), c='thistle', alpha=0.2, label='Standardabweichung')
# plt.plot(range(len(mean_energy)), mean_energy / np.nanmean(mean_energy), c='darkmagenta', label=r'Energie')
# plt.plot([270, 270], [0, mean_energy[270] / np.nanmean(mean_energy)], lw=2, c='k', linestyle='--', label='Visualisierte Zustände')
# plt.plot([285, 285], [0, mean_energy[285] / np.nanmean(mean_energy)], lw=2, c='k', linestyle='--')
# plt.plot([300, 300], [0, mean_energy[300] / np.nanmean(mean_energy)], lw=2, c='k', linestyle='--')
# plt.plot([600, 600], [0, mean_energy[600] / np.nanmean(mean_energy)], lw=2, c='k', linestyle='--')
# plt.xlabel(r'$t / \tau_\eta$')
# plt.ylabel(r'$\langle E(t)\rangle_{\Gamma, \tau_\eta} / \langle E \rangle$'),
# plt.legend(frameon=False)
# plt.show()
# =============================================================================

# =============================================================================
# Plot of velocity, acceleration and alpha

fig = plt.figure(constrained_layout=True, figsize=(10, 7), dpi=100)
gs = GridSpec(2, 2, figure=fig)
ax1 = fig.add_subplot(gs[0, 0])
ax2 = fig.add_subplot(gs[0, 1])
ax3 = fig.add_subplot(gs[1, :])

t = [i/14 for i in range(len(acf_x))]
ax1.plot(t, acf_x / acf_x[0], label='x', c='yellowgreen')
ax1.plot(t, acf_y / acf_y[0], label='y', c='steelblue')
ax1.plot(t, acf_z / acf_z[0], label='z', c='purple')
ax1.plot((0, len(acf_x)/14), (0, 0), '--', c='k')
ax1.set_ylabel(r'$C^a (\tau) / \langle a^2 \rangle$')
ax1.set_xlabel(r'$\tau / \tau_{\eta}$')
ax1.set_xlim(0, 9.5)
ax1.set_title(r'\textbf{a}', loc='left')
ax1.legend(frameon=False)

ax2.plot(bins_hist_velocity, hist_velocity_x, label=r'$v_x$', c='yellowgreen')
ax2.plot(bins_hist_velocity, hist_velocity_y, label=r'$v_y$', c='steelblue')
ax2.plot(bins_hist_velocity, hist_velocity_z, label=r'$v_z$', c='purple')
ax2.set_xlabel(r'$v / \langle v^2 \rangle^{1/2}$')
ax2.set_ylabel(r'$\langle v^2 \rangle^{1/2} f(v)$')
ax2.set_yscale('log')
ax2.yaxis.set_label_position("right")
ax2.yaxis.set_ticks_position("right")
ax2.set_title(r'\textbf{b}', loc='left')

ax3.plot(bins_hist_accelerations, hist_accelerations_x, label=r'$x$', c='yellowgreen')
ax3.plot(bins_hist_accelerations, hist_accelerations_y, label=r'$y$', c='steelblue')
ax3.plot(bins_hist_accelerations, hist_accelerations_z, label=r'$z$', c='purple')
ax3.set_xlabel(r'$a / \langle a^2 \rangle^{1/2}$')
ax3.set_ylabel(r'$\langle a^2 \rangle^{1/2} f(a)$')
ax3.set_yscale('log')
ax3.set_title(r'\textbf{c}', loc='left')
ax3.legend(frameon=False)

# ax3.plot(bins_hist_alpha, hist_alpha, c='r')
# ax3.set_xlabel(r'$\alpha / \langle \alpha \rangle$')
# ax3.set_ylabel(r'$\langle \alpha \rangle f(\alpha)$')
# ax3.set_xscale('log')
# ax3.set_yscale('log')
# ax3.set_ylim(1e-10, 100)
ax3.set_title(r'\textbf{c}', loc='left')
plt.savefig(f'{args.output}/pdf_vel_accel_full.pdf')
# plt.show()
# =============================================================================

# =============================================================================
# Plot of cond. acceleration, 2. moment and flatness

fig = plt.figure(constrained_layout=True, figsize=(10, 7), dpi=100)
gs = GridSpec(3, 2, figure=fig)
ax1 = fig.add_subplot(gs[0:2, :])
ax2 = fig.add_subplot(gs[2, 0])
ax3 = fig.add_subplot(gs[2, 1])

ax1.plot(bins_hist_accelerations, hist_accelerations_x, c='r')

for i, i_bin in enumerate([0, 2, 4, 6, 11, 17]):
    ax1.plot(bins_hist_accelerations, cond_hist_accel[i], label=f'$\\alpha = {alpha_bin_centers[i_bin]}\\langle \\alpha \\rangle$', c=colors[i])

ax1.plot(bins_hist_accelerations, np.exp(-bins_hist_accelerations**2/2)/np.sqrt(2*np.pi), '--', color='k')
ax1.set_yscale('log')
ax1.set_ylim(1e-8, 3)
ax1.set_xlabel(r'$a / \langle a^2 | \alpha \rangle^{1/2}$')
ax1.set_ylabel(r'$\langle a^2 | \alpha \rangle^{1/2} f(a | \alpha)$')
ax1.set_title(r'\textbf{a}', loc='left')
ax1.legend(frameon=False, prop=dict(size=10))

ax2.plot(alpha_bin_centers, cond_accel_rms**2 / mean_alpha, color='r')
# ax2.plot(alpha_bin_centers, 0.004*np.asarray(alpha_bin_centers), '--', color='k')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax2.set_xlabel(r'$\alpha / \langle \alpha \rangle$')
ax2.set_ylabel(r'$\langle a^2 | \alpha \rangle / \langle \alpha \rangle$')
ax2.set_title(r'\textbf{b}', loc='left')

flatness = flatness_acceleration**2 / cond_accel_rms**4
ax3.plot(alpha_bin_centers, flatness, color='r')
ax3.plot(alpha_bin_centers, 3*np.ones(18), '--', color='k')
ax3.yaxis.set_label_position("right")
ax3.yaxis.set_ticks_position("right")
ax3.set_ylim(2, 4)
for i, i_bin in enumerate([0, 2, 4, 6, 11, 17]):
    ax3.plot([alpha_bin_centers[i_bin], alpha_bin_centers[i_bin]],[0, flatness[i_bin]], color=colors[i])

ax3.set_xscale('log')
# ax3.set_yscale('log')
ax3.set_xlabel(r'$\alpha / \langle \alpha \rangle$')
ax3.set_ylabel(r'$\langle a^4 | \alpha \rangle / \langle a^2 | \alpha \rangle^2$')
ax3.set_title(r'\textbf{c}', loc='left')
plt.savefig(f'{args.output}/pdf_conf_full.pdf')
# plt.show()
# =============================================================================
