from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.progressbar import printProgressBar
import matplotlib.pyplot as plt

import numpy as np
import concurrent.futures

import os
import argparse

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=18)

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for energy files.')
parser.add_argument('-ts', '--time-start', type=int, default=0, metavar='', required=False, help='Start time (default = 0).')
parser.add_argument('-tm', '--time-max', type=int, default=15000, metavar='', required=False, help='End time (default = 15000).')
parser.add_argument('-int', '--intervall', type=int, default=14, metavar='', required=False, help='Time intervall size (default=14).')
args = parser.parse_args()
# =============================================================================

# =============================================================================
# Creating file list
filelist = list()
for subdir, dir, files in os.walk(args.input):
    for file in files:
        filelist.append((file, subdir))

# Remove all not following the naming convention
filelist = [file for file in filelist if file[0].endswith('.npz')]
# =============================================================================

# =============================================================================
# Fields and Parameters
start_time = args.time_start
max_time = args.time_max
interval = args.intervall

times = range(start_time, max_time, interval)

energies = [list() for _ in times]
# =============================================================================

print('Calculating 1/2 <u^2> for files.')

for file_index, file in enumerate(filelist):

    set = TrS()
    set.load_npz(os.path.join(file[1], file[0]))

    for i, t0 in enumerate(times):
        e = set.mean_kinetic_energy(t0, interval)
        if e:
            energies[i].extend(e)

    printProgressBar(file_index, len(filelist), prefix=f'Calculating mean energy:', suffix=f'{file_index+1} of {len(filelist)} done.', length=50)

mean_energies = [np.nanmean(en) for en in energies]
std_energies = [np.nanstd(en) for en in energies]
num_of_points = [len(en) for en in energies]

np.save(f'{args.output}/mean_energy.npy', np.asarray(mean_energies, dtype=np.float32))
np.save(f'{args.output}/std_energy.npy', np.asarray(std_energies, dtype=np.float32))
np.save(f'{args.output}/num_of_points.npy', np.asarray(num_of_points, dtype=np.float32))
