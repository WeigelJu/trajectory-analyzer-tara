from tara.trajectory import Trajectory as Tr
from tara.trajectory import TrajectorySet as TrS
from tara.progressbar import printProgressBar

import numpy as np
from concurrent.futures import ProcessPoolExecutor

import time
from tqdm import tqdm
import os
import argparse
import json



given_mean_alpha = 0.00033158738906418635
alpha_bin_centers = [0.01, 0.05, 0.1, 0.5, 1, 2, 4, 6, 8, 10, 12, 16, 20, 24, 28, 32, 48, 64]
rel_error = 0.1

cond_blocksize = 1000
block_width = 10000

# =============================================================================
# Arguments
parser = argparse.ArgumentParser(description='Calculates Root Mean Square of velocity, acceleration and alpah.')
parser.add_argument('-i', '--input', type=str, metavar='', required=True, help='Input folder with npz files.')
parser.add_argument('-c', '--clip', type=int, default=14, metavar='', help='Number of datapoints ignored at start/end of data arrays according to TrajectorySet class (default=14).')
parser.add_argument('-o', '--output', type=str, metavar='', required=True, help='Output folder for .json files.')
parser.add_argument('-dmode', '--data-mode', type=str, default='full', metavar='', help='Data mode says how much of the data will be processed: microtest, test, chunk or run or full (default is full data).')
parser.add_argument('-p', '--processes', type=int, default=1, metavar='', help='Number of files processec parallel. For optimal performance set this to (real) CPU count.')
args = parser.parse_args()
# =============================================================================

# =============================================================================
# Method to map alpha value to indices arrays for conditional statistic
def alpha_index(alpha, mean_alpha, bin_centers, relative_error):

    for index, bc in enumerate(bin_centers):

        if alpha >= mean_alpha*bc*(1 - relative_error) and alpha <= mean_alpha*bc*(1 + relative_error):
            return index

    return -1
# =============================================================================

# =============================================================================
# Creating file list
filelist = list()
for subdir, dir, files in os.walk(args.input):
    for file in files:
        filelist.append((file, subdir))

# Remove all not following the naming convention
filelist = [file for file in filelist if file[0].startswith('Run_') and file[0].endswith('.npz')]

if args.data_mode == 'microtest':
    filelist = filelist[:8]
if args.data_mode == 'test':
    filelist = filelist[:24]
if args.data_mode == 'chunk':
    filelist = [file for file in filelist if "chunk_001" in file[0] and "Run_045" in file[0]]
if args.data_mode == 'twochunks':
    filelist = [file for file in filelist if ("chunk_001" in file[0] or "chunk_002" in file[0]) and "Run_046" in file[0]]
if args.data_mode == 'run':
    filelist = [file for file in filelist if "Run_046" in file[0]]
if args.data_mode == 'full':
    filelist = filelist
# =============================================================================

# =============================================================================
def extract_veloc_accel_data(file):

    # Preparing path string to input file
    input_file_path = os.path.join(file[1], file[0])

    # Loading TrajectorySet object and setting clip
    set = TrS()
    set.load_npz(input_file_path)
    set.clip = 14

    # Global fields for RMS calculation
    # This is done by calculating the "mean" blockwise.
    global given_mean_alpha
    global alpha_bin_centers
    global rel_error

    global cond_blocksize
    global block_width

    cond_accel_rms_means = [0 for _ in alpha_bin_centers]
    cond_accel_flatness_means = [0 for _ in alpha_bin_centers]

    cond_accel_rms_blocks = [list() for _ in alpha_bin_centers]
    cond_accel_flatness_blocks = [list() for _ in alpha_bin_centers]

    cond_accel_blocks_added = [0 for _ in alpha_bin_centers]

    num_blocks_added = 0

    mean_accel = 0
    mean_veloc = 0
    mean_alpha = 0

    accel_block = list()
    veloc_block = list()
    alpha_block = list()


    # Filling the np.zero arrays with velocity and acceleration data.
    for index, traj in enumerate(set.trajectories):

        for j, a in enumerate(traj.cgl_acceleration):
            alpha_i = alpha_index(a, given_mean_alpha, alpha_bin_centers, rel_error)
            if alpha_i != -1:
                cond_accel_rms_blocks[alpha_i].append(traj.pos_dot_2[0][j]**2 + traj.pos_dot_2[1][j]**2 + traj.pos_dot_2[2][j]**2)
                cond_accel_flatness_blocks[alpha_i].append(traj.pos_dot_2[0][j]**4 + traj.pos_dot_2[1][j]**4 + traj.pos_dot_2[2][j]**4)

        veloc_block.extend((traj.pos_dot_1[0]**2 + traj.pos_dot_1[1]**2 + traj.pos_dot_1[2]**2).tolist())
        accel_block.extend((traj.pos_dot_2[0]**2 + traj.pos_dot_2[1]**2 + traj.pos_dot_2[2]**2).tolist())
        alpha_block.extend((traj.cgl_acceleration).tolist())

    # Adding
    for i, block in enumerate(cond_accel_rms_blocks):
        while len(block) >= cond_blocksize:
            mean_subblock = np.mean(block[:cond_blocksize])
            del block[:cond_blocksize]
            cond_accel_rms_means[i] += mean_subblock
            cond_accel_blocks_added[i] += 1

    for i, block in enumerate(cond_accel_flatness_blocks):
        while len(block) >= cond_blocksize:
            mean_subblock = np.mean(block[:cond_blocksize])
            del block[:cond_blocksize]
            cond_accel_flatness_means[i] += mean_subblock
            # cond_accel_blocks_added[i] += 1


    while len(alpha_block) >= block_width:

        mean_accel_subblock = np.mean(accel_block[:block_width])
        mean_veloc_subblock = np.mean(veloc_block[:block_width])
        mean_alpha_subblock = np.mean(alpha_block[:block_width])

        del accel_block[:block_width]
        del veloc_block[:block_width]
        del alpha_block[:block_width]

        mean_accel += mean_accel_subblock
        mean_veloc += mean_veloc_subblock
        mean_alpha += mean_alpha_subblock

        num_blocks_added += 1

    res_dict =	{
      "cond_accel_rms_means": cond_accel_rms_means,
      "cond_accel_rms_blocks": cond_accel_rms_blocks,
      "cond_accel_flatness_means": cond_accel_flatness_means,
      "cond_accel_flatness_blocks": cond_accel_flatness_blocks,
      "cond_accel_blocks_added": cond_accel_blocks_added,
      "num_blocks_added": num_blocks_added,
      "mean_accel": mean_accel,
      "mean_veloc": mean_veloc,
      "mean_alpha": mean_alpha,
      "accel_block": accel_block,
      "veloc_block": veloc_block,
      "alpha_block": alpha_block
    }

    out_file_path = os.path.join(args.output, file[0][:-4] +'_dict.json')

    with open(out_file_path, 'w') as fp:
        json.dump(res_dict, fp)

    return res_dict



# =============================================================================
### No Multiprocessind since the worker method edits global variables!
if __name__ == '__main__':

    print('Running 1_calculate_rms_mean_parallel.py script to calculate <alpha>, <v^2> and <a^2>.\n')

    with ProcessPoolExecutor(max_workers=args.processes) as executor:
        results = list(tqdm(executor.map(extract_veloc_accel_data, filelist), total=len(filelist)))


    cond_accel_rms_means = [0 for _ in alpha_bin_centers]
    cond_accel_flatness_means = [0 for _ in alpha_bin_centers]

    cond_accel_rms_blocks = [list() for _ in alpha_bin_centers]
    cond_accel_flatness_blocks = [list() for _ in alpha_bin_centers]

    cond_accel_blocks_added = [0 for _ in alpha_bin_centers]

    num_blocks_added = 0

    mean_accel = 0
    mean_veloc = 0
    mean_alpha = 0

    accel_block = list()
    veloc_block = list()
    alpha_block = list()

    for entry in results:

        cond_accel_rms_means = np.add(cond_accel_rms_means, entry["cond_accel_rms_means"])
        cond_accel_flatness_means = np.add(cond_accel_flatness_means, entry["cond_accel_flatness_means"])

        for i, block in enumerate(cond_accel_rms_blocks):
            block.extend(entry["cond_accel_rms_blocks"][i])

        for i, block in enumerate(cond_accel_flatness_blocks):
            block.extend(entry["cond_accel_flatness_blocks"][i])

        cond_accel_blocks_added = np.add(cond_accel_blocks_added, entry["cond_accel_blocks_added"])

        num_blocks_added += entry["num_blocks_added"]

        mean_accel += entry["mean_accel"]
        mean_veloc += entry["mean_veloc"]
        mean_alpha += entry["mean_alpha"]

        accel_block.extend(entry["accel_block"])
        veloc_block.extend(entry["veloc_block"])
        alpha_block.extend(entry["alpha_block"])

        for i, block in enumerate(cond_accel_rms_blocks):
            while len(block) >= cond_blocksize:
                mean_subblock = np.mean(block[:cond_blocksize])
                del block[:cond_blocksize]
                cond_accel_rms_means[i] += mean_subblock
                cond_accel_blocks_added[i] += 1

        for i, block in enumerate(cond_accel_flatness_blocks):
            while len(block) >= cond_blocksize:
                mean_subblock = np.mean(block[:cond_blocksize])
                del block[:cond_blocksize]
                cond_accel_flatness_means[i] += mean_subblock
                # cond_accel_blocks_added[i] += 1

        while len(alpha_block) >= block_width:

            mean_accel_subblock = np.mean(accel_block[:block_width])
            mean_veloc_subblock = np.mean(veloc_block[:block_width])
            mean_alpha_subblock = np.mean(alpha_block[:block_width])

            del accel_block[:block_width]
            del veloc_block[:block_width]
            del alpha_block[:block_width]

            mean_accel += mean_accel_subblock
            mean_veloc += mean_veloc_subblock
            mean_alpha += mean_alpha_subblock

            num_blocks_added += 1

        # print(f'RMS Acceleration = {np.sqrt(mean_accel / num_blocks_added / 3)}')
        # print(f'RMS Velocity = {np.sqrt(mean_veloc / num_blocks_added / 3)}')
        # print(f'Mean alpha = {mean_alpha / num_blocks_added}')
        # print(f'Cond RMS = {np.sqrt(np.asarray(cond_accel_means) / np.asarray(cond_accel_blocks_added))}')

    res_dict =	{
      "cond_accel_rms_means": cond_accel_rms_means.tolist(),
      "cond_accel_rms_blocks": cond_accel_rms_blocks,
      "cond_accel_flatness_means": cond_accel_flatness_means.tolist(),
      "cond_accel_flatness_blocks": cond_accel_flatness_blocks,
      "cond_accel_blocks_added": cond_accel_blocks_added.tolist(),
      "num_blocks_added": num_blocks_added,
      "mean_accel": mean_accel,
      "mean_veloc": mean_veloc,
      "mean_alpha": mean_alpha,
      "accel_block": accel_block,
      "veloc_block": veloc_block,
      "alpha_block": alpha_block
    }

    out_file_path = os.path.join(args.output, 'final_dict.json')

    with open(out_file_path, 'w') as fp:
        json.dump(res_dict, fp)


    np.save(f'{args.output}/rms_accel.npy', [np.sqrt(mean_accel / num_blocks_added / 3)])
    np.save(f'{args.output}/rms_veloc.npy', [np.sqrt(mean_veloc / num_blocks_added / 3)])
    np.save(f'{args.output}/mean_alpha.npy', [mean_alpha / num_blocks_added])
    np.save(f'{args.output}/rms_cond.npy', [np.sqrt(np.asarray(cond_accel_rms_means) / np.asarray(cond_accel_blocks_added) / 3)])
    np.save(f'{args.output}/flatness_cond.npy', [np.sqrt(np.asarray(cond_accel_flatness_means) / np.asarray(cond_accel_blocks_added) / 3)])

    print('\n')
# =============================================================================
