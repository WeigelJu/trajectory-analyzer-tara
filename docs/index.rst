.. tara documentation master file, created by
   sphinx-quickstart on Wed Sep  4 14:33:15 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tara's documentation!
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   fileconverter
   trajectory
   trajectory_class_test
   visualize


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
